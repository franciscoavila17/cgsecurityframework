﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace CG.lib.SecurityFramework.Log
{
    /// <summary>
    /// Es importante señalar los setting en el web config tal como estan a continuacion.
    /// </summary>
    public class ParamsFile
    {
        public string directory_log = System.Configuration.ConfigurationSettings.AppSettings["archivo_log_ruta"];
        public string name_log = System.Configuration.ConfigurationSettings.AppSettings["archivo_log_nombre"];
        public StreamWriter sw = null;
    }

    public class LogFile
    {
        public static void ProcessWrite(string app_name, string mensaje)
        {
            var _params = new ParamsFile();
            try
            {

                if (app_name.Length > 0)
                {
                    _params.name_log = string.Format("{1}.{2}_{0}.log", app_name, DateTime.Now.Year.ToString(),DateTime.Now.Month.ToString() );
                }



                if (Directory.Exists(_params.directory_log))
                {
                    string _full_path = string.Format("{0}{1}", _params.directory_log, _params.name_log);

                    if (!File.Exists(_full_path))
                    {
                        _params.sw = File.CreateText(_full_path);
                    }
                    else
                    {
                        _params.sw = new StreamWriter(_full_path, true);
                    }

                    //_params.sw.WriteLine("********** {0} **********", DateTime.Now);
                    //_params.sw.WriteLine(string.Format("Libreria: {0}", app_name));
                    //_params.sw.WriteLine(string.Format("{0}", mensaje));

                    //Console.WriteLine("********** {0} **********", DateTime.Now);
                    //Console.WriteLine(string.Format("Libreria: {0}", app_name));
                    //Console.WriteLine(string.Format("{0}", mensaje));

                    _params.sw.WriteLine("{0} [Lib:{1}][Msg:{2}]", DateTime.Now, app_name, mensaje);
                    Console.WriteLine("{0} [Lib:{1}][Msg:{2}]", DateTime.Now, app_name, mensaje);

                    _params.sw.Flush();
                }
            }
            catch (Exception ex)
            {

            }
            finally
            {
                if (_params.sw != null) _params.sw.Close();
            }
        }
    }
}
