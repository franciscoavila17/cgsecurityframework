﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CG.lib.SecurityFramework.Log;
using CG.lib.SecurityFramework.Cryptology;
using Owasp.Esapi;

namespace Cg.lib.SecurityFramework
{
    public static class Token
    {

        public static string GetToken(string usuario)
        {

            string _token = string.Empty;

            try
            {

                _token = string.Format("{0}-{1}",usuario,DateTime.Now.ToString("yyyy.mm.dd"));
                //Encryptor e = new Encryptor();

                _token = _token.Encrypt("LksCvWTXd8ia6NAA0aOfvQ==", "Um9zYXJpb05vcnRlTjU1NQ==");

                
            }
            catch (Exception ex)
            {
                LogFile.ProcessWrite("CG.lib.SecurityFramework.Token", ex.Message);
            }

            return _token;

        }

    }
}
