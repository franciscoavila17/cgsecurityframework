﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace CG.lib.SecurityFramework.Util
{
    public static class SQL_Util
    {

        static public bool SQLExecute( this string cmd , string conexion, ref string outError  )
        {
            bool bolErr = true;

            try
            {
                SqlConnection oConexion = new SqlConnection(conexion);
                SqlCommand oComando = new SqlCommand(cmd, oConexion);

                oConexion.Open();                
                oComando.CommandTimeout = 0;
                oComando.ExecuteScalar();
                oConexion.Close();

                oComando = null;
                oConexion = null;
                bolErr = false;
            }
            catch(Exception ex)
            {
                outError = ex.Message;
            }


            return (!bolErr);
        }


        static public bool SQLExecute(this string cmd, string conexion, ref DataSet ds,  ref string outError)
        {
            bool bolErr = true;

            try
            {

                using ( SqlConnection oConexion = new SqlConnection(conexion) )
                {                    
                    SqlDataAdapter da = new SqlDataAdapter(cmd,oConexion);
                    da.SelectCommand.CommandTimeout = 0;
                    da.Fill(ds);
                    da = null;
                    oConexion.Close();
                    oConexion.Dispose();
                }

                bolErr = false;

            }
            catch (Exception ex)
            {
                outError = ex.Message;
            }


            return (!bolErr);
        }

        static public bool SQLExecute( this SqlCommand cmd, ref string outError)
        {

            bool bolErr = true;

            try
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();
                cmd.CommandTimeout = 0;
                cmd.ExecuteScalar();

                bolErr = false;

            }
            catch (Exception ex)
            { outError = ex.Message; }
            finally
            {
                cmd.Connection.Close();                
            }

            return (!bolErr);

        }


        static public bool SQLExecute(this SqlCommand cmd, ref DataSet ds, ref string outError)
        {
            bool bolErr = true;

            try
            {

                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Connection.Open();

                SqlDataAdapter da = new SqlDataAdapter(cmd);
                da.SelectCommand.CommandTimeout = 0;
                da.Fill(ds);
                da = null;

                bolErr = false;

            }
            catch (Exception ex)
            {
                outError = ex.Message;
            }
            finally
            {
                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }


            return (!bolErr);
        }



    }
}
