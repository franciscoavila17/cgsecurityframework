﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Security.Cryptography;
using System.Runtime.Serialization.Formatters.Binary;

namespace CG.lib.SecurityFramework.Util
{
    public class Hasher : Stream
    {
        protected readonly HashAlgorithm HashAlgorithm;

        protected Hasher(HashAlgorithm hash)
        {
            HashAlgorithm = hash;
        }

        public static byte[] GetHash(object obj, HashAlgorithm hash)
        {
            var hasher = new Hasher(hash);

            if (obj != null)
            {
                var bf = new BinaryFormatter();
                bf.Serialize(hasher, obj);
            }
            else
            {
                hasher.Flush();
            }

            return hasher.HashAlgorithm.Hash;
        }

        public override bool CanRead
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanSeek
        {
            get { throw new NotImplementedException(); }
        }

        public override bool CanWrite
        {
            get { return true; }
        }

        public override void Flush()
        {
            HashAlgorithm.TransformFinalBlock(new byte[0], 0, 0);
        }

        public override long Length
        {
            get { throw new NotImplementedException(); }
        }

        public override long Position
        {
            get
            {
                throw new NotImplementedException();
            }
            set
            {
                throw new NotImplementedException();
            }
        }

        public override int Read(byte[] buffer, int offset, int count)
        {
            throw new NotImplementedException();
        }

        public override long Seek(long offset, SeekOrigin origin)
        {
            throw new NotImplementedException();
        }

        public override void SetLength(long value)
        {
            throw new NotImplementedException();
        }

        public override void Write(byte[] buffer, int offset, int count)
        {
            HashAlgorithm.TransformBlock(buffer, offset, count, buffer, offset);
        }
    }
}
