﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;

namespace CG.lib.SecurityFramework.Util
{
    public static class PDF
    {
        public static bool EstablecePassword( string ArchivoOrigen, string ArchivoDestino, string  claveUsuario, string clavePropietario) 
        {

            try
            {

                System.IO.FileStream InputFileStream = new System.IO.FileStream(ArchivoOrigen, System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.Read);
                System.IO.FileStream OutputFileStream = new System.IO.FileStream(ArchivoDestino, System.IO.FileMode.OpenOrCreate, System.IO.FileAccess.ReadWrite, System.IO.FileShare.None);
                OutputFileStream.SetLength(0);
                PdfReader PdfReader = new PdfReader(InputFileStream);
                PdfEncryptor.Encrypt(PdfReader, OutputFileStream, true, claveUsuario, clavePropietario, PdfWriter.ALLOW_PRINTING);
                InputFileStream.Close();
                InputFileStream.Dispose();                
                OutputFileStream.Close();
                OutputFileStream.Dispose();

                return true;

            }
            catch(Exception ex)
            { 
                return false;
            }

        }
    }
}
