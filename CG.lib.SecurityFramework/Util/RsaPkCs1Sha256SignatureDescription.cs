﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Security;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.ServiceModel.Channels;
using System.ServiceModel.Security;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace CG.lib.SecurityFramework.Util
{
    public class RsaPkCs1Sha256SignatureDescription : SignatureDescription
    {
        public RsaPkCs1Sha256SignatureDescription()
        {
            KeyAlgorithm = "System.Security.Cryptography.RSACryptoServiceProvider";
            DigestAlgorithm = "System.Security.Cryptography.SHA256Managed";
            FormatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureFormatter";
            DeformatterAlgorithm = "System.Security.Cryptography.RSAPKCS1SignatureDeformatter";
        }

        //public override AsymmetricSignatureDeformatter CreateDeformatter(AsymmetricAlgorithm key)
        //{
        //    var asymmetricSignatureDeformatter = (AsymmetricSignatureDeformatter)CryptoConfig.CreateFromName(DeformatterAlgorithm);
        //    asymmetricSignatureDeformatter.SetKey(key);
        //    asymmetricSignatureDeformatter.SetHashAlgorithm("SHA256");
        //    return asymmetricSignatureDeformatter;
        //}

        //public XmlElement SignDocument(XmlDocument doc, List<string> idsToSign)
        //{
        //    CryptoConfig.AddAlgorithm(typeof(RsaPkCs1Sha256SignatureDescription), @"http://www.w3.org/2001/04/xmldsig-more#rsa-sha256");

        //    var cspParams = new CspParameters(24) { KeyContainerName = "XML_DISG_RSA_KEY" };
        //    var key = new RSACryptoServiceProvider(cspParams);
        //    key.FromXmlString(_x509SecurityToken.Certificate.PrivateKey.ToXmlString(true));

        //    var signer = new SoapSignedXml(doc) { SigningKey = key };

        //    signer.SignedInfo.CanonicalizationMethod = SignedXml.XmlDsigExcC14NTransformUrl;

        //    var keyInfo = new KeyInfo();
        //    keyInfo.AddClause(new SecurityTokenReference(_x509SecurityToken, SecurityTokenReference.SerializationOptions.Embedded));

        //    signer.KeyInfo = keyInfo;
        //    signer.SignedInfo.SignatureMethod = "http://www.w3.org/2001/04/xmldsig-more#rsa-sha256";

        //    var cn14Transform = new XmlDsigExcC14NTransform();
        //    string referenceDigestMethod = "http://www.w3.org/2001/04/xmlenc#sha256";

        //    foreach (string id in idsToSign)
        //    {
        //        var reference = new Reference("#" + id);
        //        reference.AddTransform(cn14Transform);
        //        reference.DigestMethod = referenceDigestMethod;
        //        signer.AddReference(reference);
        //    }

        //    signer.ComputeSignature();

        //    return signer.GetXml();
        //}
    }
}
