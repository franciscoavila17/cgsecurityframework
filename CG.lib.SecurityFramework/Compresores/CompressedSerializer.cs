﻿using System;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using ProtoBuf;
using System.Reflection;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;
using System.Collections.Generic;
using System.Security.Cryptography;
using CG.lib.SecurityFramework.Util;
using CG.lib.SecurityFramework.Excepciones;


namespace CG.lib.SecurityFramework.Compresores
{
    public static class CompressedSerializer
    {
        static readonly IDictionary<int, Type> typeLookup = new Dictionary<int, Type> { { 1, typeof(string) }, { 2, typeof(byte[]) } };
        private static MD5 _md5 = MD5.Create();

        public static T Decompress<T>(byte[] compressedData) where T : class
        {
            try
            {
                T result = null;
                using (MemoryStream memory = new MemoryStream())
                {
                    memory.Write(compressedData, 0, compressedData.Length);
                    memory.Position = 0L;

                    var checksum = (string)ReadNext(memory);
                    var bytes = (byte[])ReadNext(memory);
                    var data = Serializer.Deserialize<T>(new MemoryStream(bytes));

                    if (data != null)
                    {
                        if (!string.Equals(checksum, Obtener_Hash(Serializer.DeepClone(data))))
                            throw new DiferenciaModeloException();
                        else
                            result = data;
                    }
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private static string Obtener_Hash<T>(T data)
        {
            return Math.Abs(BitConverter.ToInt32(_md5.ComputeHash(Hasher.GetHash(data, new MD5CryptoServiceProvider())), 0)).ToString();
        }

        public static byte[] Compress<T>(T data)
        {
            try
            {
                data = Serializer.DeepClone(data);

                byte[] bytes = ToByteArray(data);
                string checksum = Obtener_Hash(data);

                using (MemoryStream ms = new MemoryStream())
                {
                    WriteNext(ms, checksum);
                    WriteNext(ms, bytes);
                    return ms.ToArray();
                    //}
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static byte[] ToByteArray<T>(T source)
        {
            using (var stream = new MemoryStream())
            {
                Serializer.Serialize(stream, source);
                return stream.ToArray();
            }
        }

        static void WriteNext(Stream stream, object obj)
        {
            Type type = obj.GetType();
            int field = typeLookup.Single(pair => pair.Value == type).Key;
            Serializer.NonGeneric.SerializeWithLengthPrefix(stream, obj, PrefixStyle.Base128, field);
        }

        static object ReadNext(Stream stream, int pos)
        {
            object obj;
            if (Serializer.NonGeneric.TryDeserializeWithLengthPrefix(stream, PrefixStyle.Base128, field => typeLookup[pos], out obj))
            {
                return obj;
            }
            return null;
        }

        static object ReadNext(Stream stream)
        {
            object obj;
            if (Serializer.NonGeneric.TryDeserializeWithLengthPrefix(stream, PrefixStyle.Base128, field => typeLookup[field], out obj))
            {
                return obj;
            }
            return null;
        }

        static object ReadNext(Stream stream, Type type)
        {
            try
            {
                object obj;
                if (Serializer.NonGeneric.TryDeserializeWithLengthPrefix(stream, PrefixStyle.Base128, field => type, out obj))
                {
                    return obj;
                }
                return null;
            }
            catch (System.Reflection.TargetInvocationException)
            {
                return null;
            }
            catch (Exception)
            {
                throw;
            }
        }

        static byte[] ObjectToByteArray(object _Object)
        {
            try
            {
                // create new memory stream
                System.IO.MemoryStream _MemoryStream = new System.IO.MemoryStream();

                // create new BinaryFormatter
                System.Runtime.Serialization.Formatters.Binary.BinaryFormatter _BinaryFormatter
                            = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();

                // Serializes an object, or graph of connected objects, to the given stream.
                _BinaryFormatter.Serialize(_MemoryStream, _Object);

                // convert stream to byte array and return
                return _MemoryStream.ToArray();
            }
            catch (System.Runtime.Serialization.SerializationException)
            {
                throw;
            }
            catch (Exception _Exception)
            {
                // Error
                //Console.WriteLine("Exception caught in process: {0}", _Exception.ToString());
                System.Diagnostics.Debug.WriteLine(_Exception.Message, System.Reflection.MethodBase.GetCurrentMethod().Name);
            }

            // Error occured, return null
            return null;
        }

        public static string Hash_CRC32(byte[] fs)
        {
            var crc32 = new CRC32(); 
            string hash = string.Empty;

            foreach (byte b in crc32.ComputeHash(fs))
                hash += b.ToString("x2").ToLower();

            return hash;
            //return crc32.ComputeHash(fs).Aggregate(string.Empty, (current, b) => current + b.ToString("x2").ToLower());
        }
    }

    [ProtoContract]
    public class MensajeSerialization
    {
        [ProtoMember(1)]
        public string Checksum { get; set; }
        [ProtoMember(2)]
        public byte[] Bytes { get; set; }
    }
}
