﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.ComponentModel;
using System.Reflection;
using System.Data.SqlClient;
using System.Data.Linq.Mapping;
using System.IO;

namespace Corp.Framework.Helper
{
    public class CollectionHelper
    {
        private CollectionHelper()
        {
        }

        public static DataTable ConvertTo<T>(IList<T> list)
        {
            DataTable table = CreateTable<T>();
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (T item in list)
            {
                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                table.Rows.Add(row);
            }

            return table;
        }

        public static IList<T> ConvertTo<T>(IList<DataRow> rows)
        {
            IList<T> list = null;

            if (rows != null)
            {
                list = new List<T>();

                foreach (DataRow row in rows)
                {
                    T item = CreateItem<T>(row);
                    list.Add(item);
                }
            }

            return list;
        }

        public static IList<T> ConvertTo<T>(DataTable table)
        {
            if (table == null)
            {
                return null;
            }

            List<DataRow> rows = new List<DataRow>();

            foreach (DataRow row in table.Rows)
            {
                rows.Add(row);
            }

            return ConvertTo<T>(rows);
        }

        public static T CreateItem<T>(DataRow row)
        {
            T obj = default(T);
            if (row != null)
            {
                obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in row.Table.Columns)
                {
                    PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName);
                    try
                    {
                        object value = row[column.ColumnName];
                        prop.SetValue(obj, value, null);
                    }
                    catch
                    {
                        // se puede hacer log
                        throw;
                    }
                }
            }

            return obj;
        }

        public static DataTable CreateTable<T>()
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            return table;
        }

        public static DataTable ToDataTable(System.Data.Linq.DataContext ctx, object query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            IDbCommand cmd = ctx.GetCommand(query as IQueryable);            
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = (SqlCommand)cmd;
            DataTable dt = new DataTable();

            try
            {
                cmd.Connection.Open();
                adapter.FillSchema(dt, SchemaType.Source);
                adapter.Fill(dt);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return dt;
        }
        
        public static void CopiarObjetoValor<T1, T2>(T1 firstObject, T2 secondObject)
        {
            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            IEnumerable<FieldInfo> firstFieldDefinitions = firstObject.GetType().GetFields(bindingFlags);
            IEnumerable<FieldInfo> secondFieldDefinitions = secondObject.GetType().GetFields(bindingFlags);


            var result_proceso = (from x in firstFieldDefinitions
                                  from y in secondFieldDefinitions
                                  where x.Name.Equals(y.Name) & x.FieldType.Equals(y.FieldType)
                                  select new { x, y }).ToList();


            result_proceso.ForEach(p =>
            {
                var value = p.x.GetValue(firstObject);
                p.y.SetValue(secondObject, value);
            });

            //foreach (var fieldDefinition in firstFieldDefinitions)
            //{
            //    var matchingFieldDefinition = secondFieldDefinitions.FirstOrDefault(fd => fd.Name == fieldDefinition.Name &&
            //                                                                              fd.FieldType == fieldDefinition.FieldType);
            //    if (matchingFieldDefinition == null)
            //        continue;

            //    var value = fieldDefinition.GetValue(firstObject);
            //    matchingFieldDefinition.SetValue(secondObject, value);
            //}
        }

        public static bool ComprobarObjetoDiferente<T1, T2>(T1 firstObject, T2 secondObject)
        {
            bool result = false;

            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public ;
            IEnumerable<FieldInfo> firstFieldDefinitions = firstObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));
            IEnumerable<FieldInfo> secondFieldDefinitions = secondObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));

            var result_proceso = (from x in firstFieldDefinitions
                                  from y in secondFieldDefinitions
                                  where x.Name.Equals(y.Name) & x.FieldType.Equals(y.FieldType)
                                  select new { x, y}).ToList();

            foreach (var p in result_proceso)
            {
                if (result) break;
                var value_x = p.x.GetValue(firstObject);
                var value_y = p.y.GetValue(secondObject);
                if (value_x != null & value_y == null) result = true;
                else if (value_x == null & value_y != null) result = true;
                else if (value_x == null & value_y == null) result = false;
                else if (!value_x.Equals(value_y)) result = true;
                if (result)
                {
                    string quepasa = string.Empty;
                }

            }

            return result;
        }

        public static bool ComprobarObjetoDiferenteCondicional<T1, T2>(List<string> l_campos, T1 firstObject, T2 secondObject)
        {
            bool result = false;

            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            IEnumerable<FieldInfo> firstFieldDefinitions = firstObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));
            IEnumerable<FieldInfo> secondFieldDefinitions = secondObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));

            var result_proceso = (from x in firstFieldDefinitions
                                  from y in secondFieldDefinitions
                                  from z in l_campos.Select(p=> "_" + p).ToList()
                                  where x.Name.Equals(y.Name) & x.FieldType.Equals(y.FieldType) & x.Name.Equals(z)
                                  select new { x, y }).ToList();

            foreach (var p in result_proceso)
            {
                if (result) break;
                var value_x = p.x.GetValue(firstObject);
                var value_y = p.y.GetValue(secondObject);
                if (value_x != null & value_y == null) result = true;
                else if (value_x == null & value_y != null) result = true;
                else if (value_x == null & value_y == null) result = false;
                else if (!value_x.Equals(value_y)) result = true;
                if (result)
                {
                    string quepasa = string.Empty;
                }

            }

            return result;
        }

        public static dynamic ObtenerDataTableDesdeString(dynamic item)
        {
            StringReader theReader = new StringReader(item.dt_string);
            DataSet ds_ejem = new DataSet();
            ds_ejem.ReadXml(theReader);
            item.dt = ds_ejem.Tables[0];
            item.dt_string = string.Empty;
            return item;
        }

        #region ToListEspecial
    
        private static dynamic Crear_Lista(Type _tipoClase)
        {
            Type _tipo_lista = typeof(List<>).MakeGenericType(_tipoClase);
            return Activator.CreateInstance(_tipo_lista);
        }

        private static T Crear_Obj<T>(T typeofdata, Type _tipoClase)
        {
            return (T)Activator.CreateInstance(_tipoClase);
        }
                
        private static dynamic Obtener_Valor_Row<T>(DataRow row, string _campo_busqueda, Type _tipoClase, T _tipo_data)
        {
            try
            {
                PropertyInfo _campo = _tipoClase.GetProperty(_campo_busqueda);
                Type _tipo_campo = _campo.PropertyType;

                return row.Field<T>(_campo_busqueda);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static bool Setear_Campo(string _campo_busqueda, dynamic _obj, object _valor, Type _tipoClase)
        {
            try
            {
                PropertyInfo _campo = _tipoClase.GetProperty(_campo_busqueda);
                Type _tipo_campo;
                _tipo_campo = _campo.PropertyType.IsGenericType ? _campo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(_campo.PropertyType) : _campo.PropertyType : _campo.PropertyType;

                try
                {
                    _campo.SetValue(_obj, Convert.ChangeType(_valor, _tipo_campo), null);
                }
                catch
                {
                    object obj_aux = System.ComponentModel.TypeDescriptor.GetConverter(_tipo_campo).ConvertFromInvariantString(_valor.ToString());
                    _campo.SetValue(_obj, Convert.ChangeType(obj_aux, _tipo_campo), null);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static dynamic Obtener_Objeto(Type _tipoClase, DataTable dt_result)
        {
            try
            {
                dynamic lista = Crear_Lista(_tipoClase);

                DataTable dt = new DataTable();
                dt = dt_result;

                var _type_of_data = Activator.CreateInstance(_tipoClase);
                List<PropertyInfo> l_propiedades = _tipoClase.GetProperties().ToList();
                List<string> l_fields = l_propiedades.Select(p => p.Name).ToList();
                Dictionary<string, PropertyInfo> d_field_infos = (from x in l_propiedades select new { x.Name, info = x }).ToDictionary(p => p.Name, p => p.info);
                dt.AsEnumerable().ToList().ForEach(row =>
                {
                    dynamic _obj = Crear_Obj(_type_of_data, _tipoClase);
                    l_fields.ForEach(p =>
                    {
                        if (row.Table.Columns.Contains(p))
                        {
                            Type _tipo_campo = d_field_infos[p].PropertyType;
                            var _type_of_data_campo = _tipo_campo != typeof(string) ? Activator.CreateInstance(_tipo_campo) : string.Empty;

                            dynamic field_value = Obtener_Valor_Row(row, p, _tipoClase, _type_of_data_campo);
                            if (field_value != null & !string.IsNullOrEmpty(field_value)) Setear_Campo(p, _obj, field_value, _tipoClase);
                        }
                    });
                    lista.Add(_obj);
                });

                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }

    public static class LINQExtension
    {
        public static double Median<T>(this IEnumerable<T> numbers, Func<T, double> selector)
        {
            return (from num in numbers select selector(num)).Median();
        }

        public static double Median(this IEnumerable<double> source)
        {
            if (source.Count() == 0)
            {
                throw new InvalidOperationException("Cannot compute median for an empty set.");
            }

            var sortedList = from number in source
                             orderby number
                             select number;

            int itemIndex = (int)sortedList.Count() / 2;

            if (sortedList.Count() % 2 == 0)
            {
                // Even number of items. 
                return (sortedList.ElementAt(itemIndex) + sortedList.ElementAt(itemIndex - 1)) / 2;
            }
            else
            {
                // Odd number of items. 
                return sortedList.ElementAt(itemIndex);
            }
        }
        
        public static DataTable ToDataTable<T>(this IList<T> list)
        {
            DataTable dt = CollectionHelper.CreateTable<T>();
            Type entityType = typeof(T);
                        
            foreach (T item in list)
            {
                DataRow row = dt.NewRow();
                foreach (var propInfo in entityType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null);
                }

                dt.Rows.Add(row);
            }

            return dt;
        }
 
        public static string ToStringSeparator<T>(this IEnumerable<T> list, Func<T, object> selector)
        {
            return string.Join<string>(",",(from x in list select selector(x).ToString()));
        }
                
    }

    public static class EnumerableExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration"></param>
        /// <param name="action"></param>
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {

            foreach (T item in enumeration)
            {
                action(item);
            }
        }

        /// <summary>
        /// Convert  list to Data Table
        /// </summary>
        /// <typeparam name="T">Target Class</typeparam>
        /// <param name="varlist">list you want to convert it to Data Table</param>
        /// <param name="fn">Delegate Function to Create Row</param>
        /// <returns>Data Table That Represent List data</returns>
        public static DataTable ToADOTable<T>(this IEnumerable<T> varlist, CreateRowDelegate<T> fn)
        {
            DataTable toReturn = new DataTable();

            // Could add a check to verify that there is an element 0
            T TopRec = varlist.ElementAtOrDefault(0);

            if (TopRec == null)
                return toReturn;

            // Use reflection to get property names, to create table
            // column names

            PropertyInfo[] oProps = ((Type)TopRec.GetType()).GetProperties();

            foreach (PropertyInfo pi in oProps)
            {
                Type pt = pi.PropertyType;
                if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                    pt = Nullable.GetUnderlyingType(pt);
                toReturn.Columns.Add(pi.Name, pt);
            }

            foreach (T rec in varlist)
            {
                DataRow dr = toReturn.NewRow();
                foreach (PropertyInfo pi in oProps)
                {
                    object o = pi.GetValue(rec, null);
                    if (o == null)
                        dr[pi.Name] = DBNull.Value;
                    else
                        dr[pi.Name] = o;
                }
                toReturn.Rows.Add(dr);
            }

            return toReturn;
        }

        /// <summary>
        /// Convert  list to Data Table
        /// </summary>
        /// <typeparam name="T">Target Class</typeparam>
        /// <param name="varlist">list you want to convert it to Data Table</param>
        /// <returns>Data Table That Represent List data</returns>
        public static DataTable ToADOTable<T>(this IEnumerable<T> varlist)
        {
            DataTable toReturn = new DataTable();

            // Could add a check to verify that there is an element 0
            T TopRec = varlist.ElementAtOrDefault(0);

            if (TopRec == null)
                return toReturn;

            // Use reflection to get property names, to create table
            // column names

            PropertyInfo[] oProps = ((Type)TopRec.GetType()).GetProperties();

            foreach (PropertyInfo pi in oProps)
            {
                Type pt = pi.PropertyType;
                if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                    pt = Nullable.GetUnderlyingType(pt);
                toReturn.Columns.Add(pi.Name, pt);
            }

            foreach (T rec in varlist)
            {
                DataRow dr = toReturn.NewRow();
                foreach (PropertyInfo pi in oProps)
                {
                    object o = pi.GetValue(rec, null);

                    if (o == null)
                        dr[pi.Name] = DBNull.Value;
                    else
                        dr[pi.Name] = o;
                }
                toReturn.Rows.Add(dr);
            }

            return toReturn;
        }
        
        /// <summary>
        /// Convert Data Table To List of Type T
        /// </summary>
        /// <typeparam name="T">Target Class to convert data table to List of T </typeparam>
        /// <param name="datatable">Data Table you want to convert it</param>
        /// <returns>List of Target Class</returns>
        public static List<T> ToList<T>(this DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
                return Temp;
            }
            catch { return Temp; }
        }

        public static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties; Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                } return obj;
            }
            catch { return obj; }
        }

        public delegate object[] CreateRowDelegate<T>(T t);
    } 
}
