﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CG.lib.SecurityFramework.Excepciones
{
    public class DiferenciaModeloException : Exception
    {
        public DiferenciaModeloException()
        {
        }

        public DiferenciaModeloException(string message) : base(message)
        {
        }

        public DiferenciaModeloException(string message, Exception inner)
            : base(message, inner)
        {
        } 
    }

    public class ManejoExcepciones : Exception
    {
        public ManejoExcepciones()
        {
        }

        public ManejoExcepciones(string message)
            : base(message)
        {
        }

        public ManejoExcepciones(string message, Exception inner)
            : base(message, inner)
        {
        }
    }
}
