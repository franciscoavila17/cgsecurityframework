﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
namespace CG.lib.SecurityFramework.Conexion
{

    interface IConexion
    {
        BasicHttpBinding Configuracion { get; }
        EndpointAddress Endpoint { get; }
    }

    public abstract class Config_Base
    {
        public static BasicHttpBinding Configuracion
        {
            get
            {
                BasicHttpBinding binding = new BasicHttpBinding(BasicHttpSecurityMode.None);
                binding.MaxReceivedMessageSize = 67108864;
                binding.MaxBufferPoolSize = 524288;
                binding.MaxBufferSize = 67108864;
                binding.MessageEncoding = WSMessageEncoding.Text;
                binding.TextEncoding = Encoding.UTF8;
                binding.OpenTimeout = System.TimeSpan.FromMinutes(10);
                binding.SendTimeout = System.TimeSpan.FromMinutes(10);

                binding.TransferMode = TransferMode.Buffered;
                binding.ReaderQuotas.MaxDepth = 64;
                binding.ReaderQuotas.MaxStringContentLength = 67108864;
                binding.ReaderQuotas.MaxArrayLength = 67108864;
                binding.ReaderQuotas.MaxBytesPerRead = 67108864;
                binding.ReaderQuotas.MaxNameTableCharCount = 67108864;

                return binding;
            }
        }
    }

    public class Base_Url
    {
        public string _base_url;
        public Base_Url()
        { 
            this._base_url = System.Configuration.ConfigurationSettings.AppSettings["servicio_endpoint"];
        }
        public Base_Url(string wcf_apostrofe)
        {
            this._base_url = System.Configuration.ConfigurationSettings.AppSettings[string.Format("servicio_endpoint_{0}", wcf_apostrofe)];
        }
    }
}

namespace CG.lib.SecurityFramework.Conexion.Ejemplo
{
    public abstract class ConexionInfo : Config_Base
    {
        public static EndpointAddress EndpointGarantia
        {
            get
            {
                EndpointAddress endpoint = new EndpointAddress(string.Format(@"{0}sGarantia.svc", new Base_Url()._base_url));
                return endpoint;
            }
        }
    }
}

