﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CG.lib.SecurityFramework.Cryptology;

namespace CG.lib.SecurityFramework.Conexion
{
    public static class DBML
    {
        public static string ConnectionString(string connectionString)
        {
            string[] cs = connectionString.Split(';');
            List<string> l_cs = new List<string>(cs);
            var pass = l_cs.Where(p => p.Contains("Password")).FirstOrDefault().Replace("Password=","").Trim();
            connectionString = connectionString.Replace(pass.ToString(), CryptorEngine.Desencriptar(pass));

            return connectionString;
        }

        public static string CNN_STR(string bdd_prefijo)
        {
            return new Base_BDD(bdd_prefijo)._base_bdd;
        }
    }

    public class Base_BDD
    {
        public string _base_bdd;

        public Base_BDD(string bdd_prefijo)
        {
            string clave_reg_svr = System.Configuration.ConfigurationSettings.AppSettings[string.Format("{0}_SVR",bdd_prefijo)];
            string clave_reg_bdd = System.Configuration.ConfigurationSettings.AppSettings[string.Format("{0}_BDD",bdd_prefijo)];
            string clave_reg_usr = System.Configuration.ConfigurationSettings.AppSettings[string.Format("{0}_USR",bdd_prefijo)];
            string clave_reg_pwd = System.Configuration.ConfigurationSettings.AppSettings[string.Format("{0}_PWD",bdd_prefijo)];
            this._base_bdd = string.Format("Data Source={0};Initial Catalog={1};Persist Security Info=True;User ID={2};Password={3}", clave_reg_svr, clave_reg_bdd, clave_reg_usr, CryptorEngine.Desencriptar(clave_reg_pwd));
        }




    }
}
