﻿using System;
using System.ComponentModel;
using System.Data.Common;
using System.Data.Linq;
using System.Data.Linq.Mapping;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Collections.Generic;
using System.Collections.ObjectModel;

using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Globalization;
using System.Runtime.Remoting;

namespace CG.lib.SecurityFramework.Extensions
{
    public class CollectionHelper
    {
        private CollectionHelper()
        {
        }

        public static DataTable ConvertTo<T>(IList<T> list)
        {
            DataTable table = CreateTable<T>();
            Type entityType = typeof(T);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (T item in list)
            {
                DataRow row = table.NewRow();

                foreach (PropertyDescriptor prop in properties)
                {
                    row[prop.Name] = prop.GetValue(item);
                }

                table.Rows.Add(row);
            }

            return table;
        }

        public static IList<T> ConvertTo<T>(IList<DataRow> rows)
        {
            IList<T> list = null;

            if (rows != null)
            {
                list = new List<T>();

                foreach (DataRow row in rows)
                {
                    T item = CreateItem<T>(row);
                    list.Add(item);
                }
            }

            return list;
        }

        public static IList<T> ConvertTo<T>(DataTable table)
        {
            if (table == null)
            {
                return null;
            }

            List<DataRow> rows = new List<DataRow>();

            foreach (DataRow row in table.Rows)
            {
                rows.Add(row);
            }

            return ConvertTo<T>(rows);
        }

        public static T CreateItem<T>(DataRow row)
        {
            T obj = default(T);
            if (row != null)
            {
                obj = Activator.CreateInstance<T>();

                foreach (DataColumn column in row.Table.Columns)
                {
                    PropertyInfo prop = obj.GetType().GetProperty(column.ColumnName);
                    try
                    {
                        object value = row[column.ColumnName];
                        prop.SetValue(obj, value, null);
                    }
                    catch
                    {
                        // se puede hacer log
                        throw;
                    }
                }
            }

            return obj;
        }


        public static DataTable ConvertToDataTable<T>(IList<T> list)
        {
            PropertyDescriptorCollection propertyDescriptorCollection = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            for (int i = 0; i < propertyDescriptorCollection.Count; i++)
            {
                PropertyDescriptor propertyDescriptor = propertyDescriptorCollection[i];
                Type propType = propertyDescriptor.PropertyType;
                if (propType.IsGenericType && propType.GetGenericTypeDefinition() == typeof(Nullable<>))
                {
                    table.Columns.Add(propertyDescriptor.Name, Nullable.GetUnderlyingType(propType));
                }
                else
                {
                    table.Columns.Add(propertyDescriptor.Name, propType);
                }
            }
            object[] values = new object[propertyDescriptorCollection.Count];
            foreach (T listItem in list)
            {
                for (int i = 0; i < values.Length; i++)
                {
                    values[i] = propertyDescriptorCollection[i].GetValue(listItem);
                }
                table.Rows.Add(values);
            }
            return table;
        }
        
        public static DataTable CreateTable<T>()
        {
            Type entityType = typeof(T);
            DataTable table = new DataTable(entityType.Name);
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(entityType);

            foreach (PropertyDescriptor prop in properties)
            {                
                table.Columns.Add(prop.Name, prop.PropertyType);
            }

            return table;
        }

        public static DataTable ToDataTableFromQueryable(System.Data.Linq.DataContext ctx, object query)
        {
            if (query == null)
            {
                throw new ArgumentNullException("query");
            }

            IDbCommand cmd = ctx.GetCommand(query as IQueryable);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = (SqlCommand)cmd;
            DataTable dt = new DataTable();

            try
            {
                cmd.Connection.Open();
                adapter.FillSchema(dt, SchemaType.Source);
                adapter.Fill(dt);
            }
            finally
            {
                cmd.Connection.Close();
            }
            return dt;
        }
        
        public static void CopiarObjetoValor<T1, T2>(T1 firstObject, T2 secondObject)
        {
            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            IEnumerable<FieldInfo> firstFieldDefinitions = firstObject.GetType().GetFields(bindingFlags);
            IEnumerable<FieldInfo> secondFieldDefinitions = secondObject.GetType().GetFields(bindingFlags);
            
            var result_proceso = (from x in firstFieldDefinitions
                                  from y in secondFieldDefinitions
                                  where (x.Name.StartsWith("_") ? x.Name.Remove(0, 1) : x.Name).Equals(y.Name.StartsWith("_") ? y.Name.Remove(0, 1) : y.Name) & x.FieldType.Equals(y.FieldType)
                                  select new { x, y }).ToList();


            result_proceso.ForEach(p =>
            {
                var value = p.x.GetValue(firstObject);
                p.y.SetValue(secondObject, value);
            });

            //foreach (var fieldDefinition in firstFieldDefinitions)
            //{
            //    var matchingFieldDefinition = secondFieldDefinitions.FirstOrDefault(fd => fd.Name == fieldDefinition.Name &&
            //                                                                              fd.FieldType == fieldDefinition.FieldType);
            //    if (matchingFieldDefinition == null)
            //        continue;

            //    var value = fieldDefinition.GetValue(firstObject);
            //    matchingFieldDefinition.SetValue(secondObject, value);
            //}
        }

        public static bool ComprobarObjetoDiferente<T1, T2>(T1 firstObject, T2 secondObject)
        {
            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public ;
            IEnumerable<FieldInfo> firstFieldDefinitions = firstObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));
            IEnumerable<FieldInfo> secondFieldDefinitions = secondObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));

            var result_proceso = (from x in firstFieldDefinitions
                                  from y in secondFieldDefinitions
                                  where x.Name.Equals(y.Name) & x.FieldType.Equals(y.FieldType)
                                  select new { name = x.Name, value_x = x.GetValue(firstObject), value_y = y.GetValue(secondObject) }).ToList();

            var l_comparacion = (from x in result_proceso
                                 where !(x.value_x == null ? string.Empty : x.value_x.ToString().TrimEnd()).Equals(x.value_y == null ? string.Empty : x.value_y.ToString().TrimEnd())
                                 select x.name).ToList();

            return l_comparacion.Count > 0 ? true : false;


            //foreach (var p in result_proceso)
            //{
            //    if (result) break;
            //    var value_x = p.x.GetValue(firstObject);
            //    var value_y = p.y.GetValue(secondObject);
            //    if (value_x != null & value_y == null) result = true;
            //    else if (value_x == null & value_y != null) result = true;
            //    else if (value_x == null & value_y == null) result = false;
            //    else if (!value_x.Equals(value_y)) result = true;
            //    if (result)
            //    {
            //        string quepasa = string.Empty;
            //    }

            //}
           
        }

        public static bool ComprobarObjetoDiferenteCondicional<T1, T2>(List<string> l_campos, T1 firstObject, T2 secondObject)
        {

            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            IEnumerable<FieldInfo> firstFieldDefinitions = firstObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));
            IEnumerable<FieldInfo> secondFieldDefinitions = secondObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));

            var result_proceso = (from x in firstFieldDefinitions
                                  from y in secondFieldDefinitions
                                  from z in l_campos.Select(p=> "_" + p).ToList()
                                  where x.Name.Equals(y.Name) & x.FieldType.Equals(y.FieldType) & x.Name.Equals(z)
                                  select new { name = x.Name, value_x = x.GetValue(firstObject), value_y = y.GetValue(secondObject) }).ToList();

            var l_comparacion = (from x in result_proceso
                                 where !(x.value_x == null ? string.Empty : x.value_x.ToString().TrimEnd()).Equals(x.value_y == null ? string.Empty : x.value_y.ToString().TrimEnd())
                                 select x.name).ToList();

            return l_comparacion.Count > 0 ? true : false;

            //foreach (var p in result_proceso)
            //{
            //    if (result) break;
            //    var value_x = p.x.GetValue(firstObject);
            //    var value_y = p.y.GetValue(secondObject);
            //    if (value_x != null & value_y == null) result = true;
            //    else if (value_x == null & value_y != null) result = true;
            //    else if (value_x == null & value_y == null) result = false;
            //    else if (!value_x.Equals(value_y)) result = true;
            //    if (result)
            //    {
            //        string quepasa = string.Empty;
            //    }

            //}
        }

        public static bool ComprobarObjetoDiferentePK<T1, T2>(List<string> l_campos_primary_key, T1 firstObject, T2 secondObject)
        {
            const BindingFlags bindingFlags = BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public;
            IEnumerable<FieldInfo> firstFieldDefinitions = firstObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));
            IEnumerable<FieldInfo> secondFieldDefinitions = secondObject.GetType().GetFields(bindingFlags).Where(p => !p.FieldType.Name.Contains("EntityRef") & !p.FieldType.FullName.Contains("EventHandler"));

            var result_proceso = (from x in firstFieldDefinitions
                                  from y in secondFieldDefinitions
                                  from z in l_campos_primary_key.Select(p => "_" + p).ToList()
                                  where x.Name.Equals(y.Name) & x.FieldType.Equals(y.FieldType) & !x.Name.Equals(z)
                                  select new { name = x.Name, value_x = x.GetValue(firstObject), value_y = y.GetValue(secondObject) }).ToList();

            var l_comparacion = (from x in result_proceso
                                 where !(x.value_x == null ? string.Empty : x.value_x.ToString().TrimEnd()).Equals(x.value_y == null ? string.Empty : x.value_y.ToString().TrimEnd())
                                 select x.name).ToList();
            
            return l_comparacion.Count > 0 ? true : false;
        }

        public static dynamic ObtenerDataTableDesdeString(dynamic item)
        {
            StringReader theReader = new StringReader(item.dt_string);
            DataSet ds_ejem = new DataSet();
            ds_ejem.ReadXml(theReader);
            item.dt = ds_ejem.Tables[0];
            item.dt_string = string.Empty;
            return item;
        }


        #region ToListEspecial
    
        private static dynamic Crear_Lista(Type _tipoClase)
        {
            Type _tipo_lista = typeof(List<>).MakeGenericType(_tipoClase);
            return Activator.CreateInstance(_tipo_lista);
        }

        private static T Crear_Obj<T>(T typeofdata, Type _tipoClase)
        {
            return (T)Activator.CreateInstance(_tipoClase);
        }
                
        private static dynamic Obtener_Valor_Row<T>(DataRow row, string _campo_busqueda, Type _tipoClase, T _tipo_data)
        {
            try
            {
                PropertyInfo _campo = _tipoClase.GetProperty(_campo_busqueda);
                Type _tipo_campo = _campo.PropertyType;

                return row.Field<T>(_campo_busqueda);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private static bool Setear_Campo(string _campo_busqueda, dynamic _obj, object _valor, Type _tipoClase)
        {
            try
            {
                PropertyInfo _campo = _tipoClase.GetProperty(_campo_busqueda);
                Type _tipo_campo;
                _tipo_campo = _campo.PropertyType.IsGenericType ? _campo.PropertyType.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(_campo.PropertyType) : _campo.PropertyType : _campo.PropertyType;

                try
                {
                    _campo.SetValue(_obj, Convert.ChangeType(_valor, _tipo_campo), null);
                }
                catch
                {
                    object obj_aux = System.ComponentModel.TypeDescriptor.GetConverter(_tipo_campo).ConvertFromInvariantString(_valor.ToString());
                    _campo.SetValue(_obj, Convert.ChangeType(obj_aux, _tipo_campo), null);
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static dynamic Obtener_Objeto(Type _tipoClase, DataTable dt_result)
        {
            try
            {
                dynamic lista = Crear_Lista(_tipoClase);

                DataTable dt = new DataTable();
                dt = dt_result;

                var _type_of_data = Activator.CreateInstance(_tipoClase);
                List<PropertyInfo> l_propiedades = _tipoClase.GetProperties().ToList();
                List<string> l_fields = l_propiedades.Select(p => p.Name).ToList();
                Dictionary<string, PropertyInfo> d_field_infos = (from x in l_propiedades select new { x.Name, info = x }).ToDictionary(p => p.Name, p => p.info);
                dt.AsEnumerable().ToList().ForEach(row =>
                {
                    dynamic _obj = Crear_Obj(_type_of_data, _tipoClase);
                    l_fields.ForEach(p =>
                    {   
                        if (row.Table.Columns.Contains(p))
                        {
                            Type _tipo_campo = d_field_infos[p].PropertyType;
                            var _type_of_data_campo = _tipo_campo != typeof(string) ? Activator.CreateInstance(_tipo_campo) : string.Empty;

                            dynamic field_value = Obtener_Valor_Row(row, p, _tipoClase, _type_of_data_campo);
                            if (field_value != null & !string.IsNullOrEmpty(field_value)) Setear_Campo(p, _obj, field_value, _tipoClase);
                        }
                    });
                    lista.Add(_obj);
                });

                return lista;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion
    }

    public static class ObjectExtensions
    {
        public static bool Is(this object value)
        {
            return (value != null);
        }

        public static bool IsNotEmpty(this object[] value)
        {
            return ((value != null) && (value.Length != 0));
        }

        public static bool IsNull(this object value)
        {
            return (value == null);
        }

        public static string ToStringNumeric(this object value, string number_decimal)
        {
            //return IsNumeric(value) ? string.Format("{0:n2}", value) : "";
            string n_decimal = string.Format("0:n{0}", number_decimal);
            string formato = "{" + n_decimal + "}";
            return IsNumeric(value) ? string.Format(formato, value) : "";
        }

        public static System.Boolean IsNumeric(this System.Object Expression)
        {
            if (Expression == null || Expression is DateTime)
                return false;

            if (Expression is Int16 || Expression is Int32 || Expression is Int64 || Expression is Decimal || Expression is Single || Expression is Double || Expression is Boolean)
                return true;

            try
            {
                if (Expression is string)
                    Double.Parse(Expression as string);
                else
                    Double.Parse(Expression.ToString());
                return true;
            }
            catch { } // just dismiss errors but return false
            return false;
        }

    }
       

    public static class LINQExtension
    {
        public static double Median<T>(this IEnumerable<T> numbers, Func<T, double> selector)
        {
            return (from num in numbers select selector(num)).Median();
        }

        public static double Median(this IEnumerable<double> source)
        {
            if (source.Count() == 0)
            {
                throw new InvalidOperationException("Cannot compute median for an empty set.");
            }

            var sortedList = from number in source
                             orderby number
                             select number;

            int itemIndex = (int)sortedList.Count() / 2;

            if (sortedList.Count() % 2 == 0)
            {
                // Even number of items. 
                return (sortedList.ElementAt(itemIndex) + sortedList.ElementAt(itemIndex - 1)) / 2;
            }
            else
            {
                // Odd number of items. 
                return sortedList.ElementAt(itemIndex);
            }
        }
        
        public static DataTable ToDataTableFromGeneric<T>(this IList<T> list)
        {
            DataTable dt = CollectionHelper.CreateTable<T>();
            Type entityType = typeof(T);    
                        
            foreach (T item in list)
            {
                DataRow row = dt.NewRow();
                foreach (var propInfo in entityType.GetProperties())
                {
                    row[propInfo.Name] = propInfo.GetValue(item, null);
                }

                dt.Rows.Add(row);
            }

            return dt;
        }

        public static DataTable     ToDataTableFromAnonymous<T>(this IEnumerable<T> varlist)
        {
            DataTable dtReturn = new DataTable();

            PropertyInfo[] oProps = null;

            if (varlist == null) return dtReturn;

            Type itemType = null;
            Type type = varlist.GetType();
            if (type.IsGenericType && type.GetGenericTypeDefinition()
                    == typeof(List<>))
            {
                itemType = type.GetGenericArguments()[0]; // use this...
            }

            if (itemType != null)
            {
                oProps = itemType.GetProperties();
                foreach (PropertyInfo pi in oProps)
                {
                    Type colType = pi.PropertyType;
                    if ((colType.IsGenericType) && (colType.GetGenericTypeDefinition() == typeof(Nullable<>)))
                    {
                        colType = colType.GetGenericArguments()[0];
                    }
                    dtReturn.Columns.Add(new DataColumn(pi.Name, colType));
                }
            }

            foreach (T rec in varlist)
            {
                DataRow dr = dtReturn.NewRow();
                foreach (PropertyInfo pi in oProps)
                {
                    dr[pi.Name] = pi.GetValue(rec, null) == null ? DBNull.Value : pi.GetValue
                    (rec, null);
                }
                dtReturn.Rows.Add(dr);
            }
            return dtReturn;
        }
 
        public static string ToStringSeparator<T>(this IEnumerable<T> list, Func<T, object> selector)
        {
            return string.Join<string>(",",(from x in list select selector(x).ToString()));
        }
                
    }

    #region LinqCRUD
    
    public static class LinqToSqlExtensions
    {
        /// <summary>
        /// Creates a *.csv file from an IQueryable query, dumping out the 'simple' properties/fields.
        /// </summary>
        /// <param name="query">Represents a SELECT query to execute.</param>
        /// <param name="fileName">The name of the file to create.</param>
        /// <remarks>
        /// <para>If the file specified by <paramref name="fileName"/> exists, it will be deleted.</para>
        /// <para>If the <paramref name="query"/> contains any properties that are entity sets (i.e. rows from a FK relationship) the values will not be dumped to the file.</para>
        /// <para>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</para>
        /// </remarks>
        public static void DumpCSV(this IQueryable query, string fileName)
        {
            query.DumpCSV(fileName, true);
        }

        /// <summary>
        /// Creates a *.csv file from an IQueryable query, dumping out the 'simple' properties/fields.
        /// </summary>
        /// <param name="query">Represents a SELECT query to execute.</param>
        /// <param name="fileName">The name of the file to create.</param>
        /// <param name="deleteFile">Whether or not to delete the file specified by <paramref name="fileName"/> if it exists.</param>
        /// <remarks>
        /// <para>If the <paramref name="query"/> contains any properties that are entity sets (i.e. rows from a FK relationship) the values will not be dumped to the file.</para>
        /// <para>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</para>
        /// </remarks>
        public static void DumpCSV(this IQueryable query, string fileName, bool deleteFile)
        {
            if (File.Exists(fileName) && deleteFile)
            {
                File.Delete(fileName);
            }

            using (var output = new FileStream(fileName, FileMode.CreateNew))
            {
                using (var writer = new StreamWriter(output))
                {
                    var firstRow = true;

                    PropertyInfo[] properties = null;
                    FieldInfo[] fields = null;
                    Type type = null;
                    bool typeIsAnonymous = false;

                    foreach (var r in query)
                    {
                        if (type == null)
                        {
                            type = r.GetType();
                            typeIsAnonymous = type.IsAnonymous();
                            properties = type.GetProperties();
                            fields = type.GetFields();
                        }

                        var firstCol = true;

                        if (typeIsAnonymous)
                        {
                            if (firstRow)
                            {
                                foreach (var p in properties)
                                {
                                    if (!firstCol) writer.Write(",");
                                    else { firstCol = false; }

                                    writer.Write(p.Name);
                                }
                                writer.WriteLine();
                            }
                            firstRow = false;
                            firstCol = true;

                            foreach (var p in properties)
                            {
                                if (!firstCol) writer.Write(",");
                                else { firstCol = false; }
                                DumpValue(p.GetValue(r, null), writer);
                            }
                        }
                        else
                        {
                            if (firstRow)
                            {
                                foreach (var p in fields)
                                {
                                    if (!firstCol) writer.Write(",");
                                    else { firstCol = false; }

                                    writer.Write(p.Name);
                                }
                                writer.WriteLine();
                            }
                            firstRow = false;
                            firstCol = true;

                            foreach (var p in fields)
                            {
                                if (!firstCol) writer.Write(",");
                                else { firstCol = false; }

                                DumpValue(p.GetValue(r), writer);
                            }
                        }

                        writer.WriteLine();
                    }
                }
            }
        }

        private static void DumpValue(object v, StreamWriter writer)
        {
            if (v != null)
            {
                switch (Type.GetTypeCode(v.GetType()))
                {
                    // csv encode the value
                    case TypeCode.String:
                        string value = (string)v;
                        if (value.Contains(",") || value.Contains('"') || value.Contains("\n"))
                        {
                            value = value.Replace("\"", "\"\"");

                            if (value.Length > 31735)
                            {
                                value = value.Substring(0, 31732) + "...";
                            }
                            writer.Write("\"" + value + "\"");
                        }
                        else
                        {
                            writer.Write(value);
                        }
                        break;

                    default: writer.Write(v); break;
                }
            }
        }

        private static bool IsAnonymous(this Type type)
        {
            if (type == null)
                throw new ArgumentNullException("type");

            // HACK: The only way to detect anonymous types right now.
            return Attribute.IsDefined(type, typeof(CompilerGeneratedAttribute), false)
                       && type.IsGenericType && type.Name.Contains("AnonymousType")
                       && (type.Name.StartsWith("<>") || type.Name.StartsWith("VB$"))
                       && (type.Attributes & TypeAttributes.NotPublic) == TypeAttributes.NotPublic;

        }

        /// <summary>
        /// Batches together multiple IQueryable queries into a single DbCommand and returns all data in
        /// a single roundtrip to the database.
        /// </summary>
        /// <param name="context">The DataContext to execute the batch select against.</param>
        /// <param name="queries">Represents a collections of SELECT queries to execute.</param>
        /// <returns>Returns an IMultipleResults object containing all results.</returns>
        public static IMultipleResults SelectMutlipleResults(this DataContext context, IQueryable[] queries)
        {
            var commandList = new List<DbCommand>();

            foreach (IQueryable query in queries)
            {
                var command = context.GetCommand(query);
                commandList.Add(command);
            }

            SqlCommand batchCommand = CombineCommands(commandList);
            batchCommand.Connection = context.Connection as SqlConnection;

            DbDataReader dr = null;

            if (batchCommand.Connection.State == ConnectionState.Closed)
            {
                batchCommand.Connection.Open();
                dr = batchCommand.ExecuteReader(CommandBehavior.CloseConnection);
            }
            else
            {
                dr = batchCommand.ExecuteReader();
            }

            IMultipleResults mr = context.Translate(dr);
            return mr;
        }

        /// <summary>
        /// Combines multiple SELECT commands into a single SqlCommand so that all statements can be executed in a
        /// single roundtrip to the database and return multiple result sets.
        /// </summary>
        /// <param name="commandList">Represents a collection of commands to be batched together.</param>
        /// <returns>Returns a single SqlCommand that executes all SELECT statements at once.</returns>
        private static SqlCommand CombineCommands(List<DbCommand> selectCommands)
        {
            SqlCommand batchCommand = new SqlCommand();
            SqlParameterCollection newParamList = batchCommand.Parameters;

            int commandCount = 0;

            foreach (DbCommand cmd in selectCommands)
            {
                string commandText = cmd.CommandText;
                DbParameterCollection paramList = cmd.Parameters;
                int paramCount = paramList.Count;

                for (int currentParam = paramCount - 1; currentParam >= 0; currentParam--)
                {
                    DbParameter param = paramList[currentParam];
                    DbParameter newParam = CloneParameter(param);
                    string newParamName = param.ParameterName.Replace("@", string.Format("@{0}_", commandCount));
                    commandText = commandText.Replace(param.ParameterName, newParamName);
                    newParam.ParameterName = newParamName;
                    newParamList.Add(newParam);
                }
                if (batchCommand.CommandText.Length > 0)
                {
                    batchCommand.CommandText += ";";
                }
                batchCommand.CommandText += commandText;
                commandCount++;
            }

            return batchCommand;
        }

        /// <summary>
        /// Returns a clone (via copying all properties) of an existing DbParameter.
        /// </summary>
        /// <param name="src">The DbParameter to clone.</param>
        /// <returns>Returns a clone (via copying all properties) of an existing DbParameter.</returns>
        private static DbParameter CloneParameter(DbParameter src)
        {
            SqlParameter source = (SqlParameter)src;
            SqlParameter destination = new SqlParameter();

            destination.Value = source.Value;
            destination.Direction = source.Direction;
            destination.Size = source.Size;
            destination.Offset = source.Offset;
            destination.SourceColumn = source.SourceColumn;
            destination.SourceVersion = source.SourceVersion;
            destination.SourceColumnNullMapping = source.SourceColumnNullMapping;
            destination.IsNullable = source.IsNullable;

            destination.CompareInfo = source.CompareInfo;
            destination.XmlSchemaCollectionDatabase = source.XmlSchemaCollectionDatabase;
            destination.XmlSchemaCollectionOwningSchema = source.XmlSchemaCollectionOwningSchema;
            destination.XmlSchemaCollectionName = source.XmlSchemaCollectionName;
            destination.UdtTypeName = source.UdtTypeName;
            destination.TypeName = source.TypeName;
            destination.ParameterName = source.ParameterName;
            destination.Precision = source.Precision;
            destination.Scale = source.Scale;

            return destination;
        }

        /// <summary>
        /// Immediately deletes all entities from the collection with a single delete command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <typeparam name="TPrimaryKey">Represents the object type for the primary key of rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="primaryKey">Represents the primary key of the item to be removed from <paramref name="table"/>.</param>
        /// <returns>The number of rows deleted from the database (maximum of 1).</returns>
        /// <remarks>
        /// <para>If the primary key for <paramref name="table"/> is a composite key, <paramref name="primaryKey"/> should be an anonymous type with property names mapping to the property names of objects of type <typeparamref name="TEntity"/>.</para>
        /// </remarks>
        public static int DeleteByPK<TEntity>(this Table<TEntity> table, object primaryKey) where TEntity : class
        {
            DbCommand delete = table.GetDeleteByPKCommand<TEntity>(primaryKey);

            var parameters = from p in delete.Parameters.Cast<DbParameter>()
                             select p.Value;

            return table.Context.ExecuteCommand(delete.CommandText, parameters.ToArray());
        }

        /// <summary>Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete a entity row via the supplied primary key.</summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <typeparam name="TPrimaryKey">Represents the object type for the primary key of rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="primaryKey">Represents the primary key of the item to be removed from <paramref name="table"/>.</param>
        /// <returns>Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete a entity row via the supplied primary key.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        public static string DeleteByPKPreview<TEntity>(this Table<TEntity> table, object primaryKey) where TEntity : class
        {
            DbCommand delete = table.GetDeleteByPKCommand<TEntity>(primaryKey);
            return delete.PreviewCommandText(false) + table.Context.GetLog();
        }

        private static DbCommand GetDeleteByPKCommand<TEntity>(this Table<TEntity> table, object primaryKey) where TEntity : class
        {
            Type type = primaryKey.GetType();
            bool typeIsAnonymous = type.IsAnonymous();
            string dbName = table.GetDbName();

            var metaTable = table.Context.Mapping.GetTable(typeof(TEntity));

            var keys = from mdm in metaTable.RowType.DataMembers
                       where mdm.IsPrimaryKey
                       select new { mdm.MappedName, mdm.Name, mdm.Type };

            SqlCommand deleteCommand = new SqlCommand();
            deleteCommand.Connection = table.Context.Connection as SqlConnection;

            var whereSB = new StringBuilder();

            foreach (var key in keys)
            {
                // Add new parameter with massaged name to avoid clashes.
                whereSB.AppendFormat("[{0}] = @p{1}, ", key.MappedName, deleteCommand.Parameters.Count);

                object value = primaryKey;
                if (typeIsAnonymous || (type.IsClass && type != typeof(string)))
                {
                    if (typeIsAnonymous)
                    {
                        PropertyInfo property = type.GetProperty(key.Name);

                        if (property == null)
                        {
                            throw new ArgumentOutOfRangeException(string.Format("The property {0} which is defined as part of the primary key for {1} was not supplied by the parameter primaryKey.", key.Name, metaTable.TableName));
                        }

                        value = property.GetValue(primaryKey, null);
                    }
                    else
                    {
                        FieldInfo field = type.GetField(key.Name);

                        if (field == null)
                        {
                            throw new ArgumentOutOfRangeException(string.Format("The property {0} which is defined as part of the primary key for {1} was not supplied by the parameter primaryKey.", key.Name, metaTable.TableName));
                        }

                        value = field.GetValue(primaryKey);
                    }

                    if (value.GetType() != key.Type)
                    {
                        throw new InvalidCastException(string.Format("The property {0} ({1}) does not have the same type as {2} ({3}).", key.Name, value.GetType(), key.MappedName, key.Type));
                    }
                }
                else if (value.GetType() != key.Type)
                {
                    throw new InvalidCastException(string.Format("The value supplied in primaryKey ({0}) does not have the same type as {1} ({2}).", value.GetType(), key.MappedName, key.Type));
                }

                deleteCommand.Parameters.Add(new SqlParameter(string.Format("@p{0}", deleteCommand.Parameters.Count), value));
            }

            string wherePK = whereSB.ToString();

            if (wherePK == "")
            {
                throw new MissingPrimaryKeyException(string.Format("{0} does not have a primary key defined.  Batch updating/deleting can not be used for tables without a primary key.", metaTable.TableName));
            }

            deleteCommand.CommandText = string.Format("DELETE {0}\r\nWHERE {1}", dbName, wherePK.Substring(0, wherePK.Length - 2));

            return deleteCommand;
        }

        /// <summary>
        /// Immediately deletes all entities from the collection with a single delete command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <param name="entities">Represents the collection of items which are to be removed from <paramref name="table"/>.</param>
        /// <returns>The number of rows deleted from the database.</returns>
        /// <remarks>
        /// <para>Similiar to stored procedures, and opposite from DeleteAllOnSubmit, rows provided in <paramref name="entities"/> will be deleted immediately with no need to call <see cref="DataContext.SubmitChanges()"/>.</para>
        /// <para>Additionally, to improve performance, instead of creating a delete command for each item in <paramref name="entities"/>, a single delete command is created.</para>
        /// </remarks>
        public static int DeleteBatch<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities) where TEntity : class
        {
            DbCommand delete = table.GetDeleteBatchCommand<TEntity>(entities);

            var parameters = from p in delete.Parameters.Cast<DbParameter>()
                             select p.Value;

            return table.Context.ExecuteCommand(delete.CommandText, parameters.ToArray());
        }

        /// <summary>
        /// Immediately deletes all entities from the collection with a single delete command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <param name="filter">Represents a filter of items to be updated in <paramref name="table"/>.</param>
        /// <returns>The number of rows deleted from the database.</returns>
        /// <remarks>
        /// <para>Similiar to stored procedures, and opposite from DeleteAllOnSubmit, rows provided in <paramref name="entities"/> will be deleted immediately with no need to call <see cref="DataContext.SubmitChanges()"/>.</para>
        /// <para>Additionally, to improve performance, instead of creating a delete command for each item in <paramref name="entities"/>, a single delete command is created.</para>
        /// </remarks>
        public static int DeleteBatch<TEntity>(this Table<TEntity> table, Expression<Func<TEntity, bool>> filter) where TEntity : class
        {
            return table.DeleteBatch(table.Where(filter));
        }

        /// <summary>
        /// Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <param name="entities">Represents the collection of items which are to be removed from <paramref name="table"/>.</param>
        /// <returns>Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        public static string DeleteBatchPreview<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities) where TEntity : class
        {
            DbCommand delete = table.GetDeleteBatchCommand<TEntity>(entities);
            return delete.PreviewCommandText(false) + table.Context.GetLog();
        }

        /// <summary>
        /// Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <param name="filter">Represents a filter of items to be updated in <paramref name="table"/>.</param>
        /// <returns>Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        public static string DeleteBatchPreview<TEntity>(this Table<TEntity> table, Expression<Func<TEntity, bool>> filter) where TEntity : class
        {
            return table.DeleteBatchPreview(table.Where(filter));
        }

        /// <summary>
        /// Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <param name="entities">Represents the collection of items which are to be removed from <paramref name="table"/>.</param>
        /// <returns>Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.</returns>
        /// <remarks>This method is useful for debugging purposes or when LINQ generated queries need to be passed to developers without LINQ/LINQPad.</remarks>
        public static string DeleteBatchSQL<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities) where TEntity : class
        {
            DbCommand delete = table.GetDeleteBatchCommand<TEntity>(entities);
            return delete.PreviewCommandText(true);
        }

        /// <summary>
        /// Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be deleted.</param>
        /// <param name="filter">Represents a filter of items to be updated in <paramref name="table"/>.</param>
        /// <returns>Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to delete all entities from the collection with a single delete command.</returns>
        /// <remarks>This method is useful for debugging purposes or when LINQ generated queries need to be passed to developers without LINQ/LINQPad.</remarks>
        public static string DeleteBatchSQL<TEntity>(this Table<TEntity> table, Expression<Func<TEntity, bool>> filter) where TEntity : class
        {
            return table.DeleteBatchSQL(table.Where(filter));
        }

        /// <summary>
        /// Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be updated.</param>
        /// <param name="entities">Represents the collection of items which are to be updated in <paramref name="table"/>.</param>
        /// <param name="evaluator">A Lambda expression returning a <typeparamref name="TEntity"/> that defines the update assignments to be performed on each item in <paramref name="entities"/>.</param>
        /// <returns>Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        public static string UpdateBatchPreview<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities, Expression<Func<TEntity, TEntity>> evaluator) where TEntity : class
        {
            DbCommand update = table.GetUpdateBatchCommand<TEntity>(entities, evaluator);
            return update.PreviewCommandText(false) + table.Context.GetLog();
        }

        /// <summary>
        /// Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be updated.</param>
        /// <param name="filter">Represents a filter of items to be updated in <paramref name="table"/>.</param>
        /// <param name="evaluator">A Lambda expression returning a <typeparamref name="TEntity"/> that defines the update assignments to be performed on each item in <paramref name="entities"/>.</param>
        /// <returns>Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        public static string UpdateBatchPreview<TEntity>(this Table<TEntity> table, Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TEntity>> evaluator) where TEntity : class
        {
            return table.UpdateBatchPreview(table.Where(filter), evaluator);
        }

        /// <summary>
        /// Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be updated.</param>
        /// <param name="entities">Represents the collection of items which are to be updated in <paramref name="table"/>.</param>
        /// <param name="evaluator">A Lambda expression returning a <typeparamref name="TEntity"/> that defines the update assignments to be performed on each item in <paramref name="entities"/>.</param>
        /// <returns>Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.</returns>
        /// <remarks>This method is useful for debugging purposes or when LINQ generated queries need to be passed to developers without LINQ/LINQPad.</remarks>
        public static string UpdateBatchSQL<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities, Expression<Func<TEntity, TEntity>> evaluator) where TEntity : class
        {
            DbCommand update = table.GetUpdateBatchCommand<TEntity>(entities, evaluator);
            return update.PreviewCommandText(true);
        }

        /// <summary>
        /// Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be updated.</param>
        /// <param name="filter">Represents a filter of items to be updated in <paramref name="table"/>.</param>
        /// <param name="evaluator">A Lambda expression returning a <typeparamref name="TEntity"/> that defines the update assignments to be performed on each item in <paramref name="entities"/>.</param>
        /// <returns>Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to update all entities from the collection with a single update command.</returns>
        /// <remarks>This method is useful for debugging purposes or when LINQ generated queries need to be passed to developers without LINQ/LINQPad.</remarks>
        public static string UpdateBatchSQL<TEntity>(this Table<TEntity> table, Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TEntity>> evaluator) where TEntity : class
        {
            return table.UpdateBatchSQL(table.Where(filter), evaluator);
        }

        /// <summary>
        /// Immediately updates all entities in the collection with a single update command based on a <typeparamref name="TEntity"/> created from a Lambda expression.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be updated.</param>
        /// <param name="entities">Represents the collection of items which are to be updated in <paramref name="table"/>.</param>
        /// <param name="evaluator">A Lambda expression returning a <typeparamref name="TEntity"/> that defines the update assignments to be performed on each item in <paramref name="entities"/>.</param>
        /// <returns>The number of rows updated in the database.</returns>
        /// <remarks>
        /// <para>Similiar to stored procedures, and opposite from similiar InsertAllOnSubmit, rows provided in <paramref name="entities"/> will be updated immediately with no need to call <see cref="DataContext.SubmitChanges()"/>.</para>
        /// <para>Additionally, to improve performance, instead of creating an update command for each item in <paramref name="entities"/>, a single update command is created.</para>
        /// </remarks>
        public static int UpdateBatch<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities, Expression<Func<TEntity, TEntity>> evaluator) where TEntity : class
        {
            DbCommand update = table.GetUpdateBatchCommand<TEntity>(entities, evaluator);

            var parameters = from p in update.Parameters.Cast<DbParameter>()
                             select p.Value;
            return table.Context.ExecuteCommand(update.CommandText, parameters.ToArray());
        }

        /// <summary>
        /// Immediately updates all entities in the collection with a single update command based on a <typeparamref name="TEntity"/> created from a Lambda expression.
        /// </summary>
        /// <typeparam name="TEntity">Represents the object type for rows contained in <paramref name="table"/>.</typeparam>
        /// <param name="table">Represents a table for a particular type in the underlying database containing rows are to be updated.</param>
        /// <param name="filter">Represents a filter of items to be updated in <paramref name="table"/>.</param>
        /// <param name="evaluator">A Lambda expression returning a <typeparamref name="TEntity"/> that defines the update assignments to be performed on each item in <paramref name="entities"/>.</param>
        /// <returns>The number of rows updated in the database.</returns>
        /// <remarks>
        /// <para>Similiar to stored procedures, and opposite from similiar InsertAllOnSubmit, rows provided in <paramref name="entities"/> will be updated immediately with no need to call <see cref="DataContext.SubmitChanges()"/>.</para>
        /// <para>Additionally, to improve performance, instead of creating an update command for each item in <paramref name="entities"/>, a single update command is created.</para>
        /// </remarks>
        public static int UpdateBatch<TEntity>(this Table<TEntity> table, Expression<Func<TEntity, bool>> filter, Expression<Func<TEntity, TEntity>> evaluator) where TEntity : class
        {
            return table.UpdateBatch(table.Where(filter), evaluator);
        }

        /// <summary>
        /// Returns the Transact SQL string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to perform the query's select statement.
        /// </summary>
        /// <param name="context">The DataContext to execute the batch select against.</param>
        /// <param name="query">Represents the SELECT query to execute.</param>
        /// <returns>Returns the Transact SQL string representation of the <see cref="DbCommand.CommandText"/> along with <see cref="DbCommand.Parameters"/> if present.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        public static string PreviewSQL(this DataContext context, IQueryable query)
        {
            var cmd = context.GetCommand(query);
            return cmd.PreviewCommandText(true);
        }

        /// <summary>
        /// Returns a string representation the LINQ <see cref="IProvider"/> command text and parameters used that would be issued to perform the query's select statement.
        /// </summary>
        /// <param name="context">The DataContext to execute the batch select against.</param>
        /// <param name="query">Represents the SELECT query to execute.</param>
        /// <returns>Returns a string representation of the <see cref="DbCommand.CommandText"/> along with <see cref="DbCommand.Parameters"/> if present.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        public static string PreviewCommandText(this DataContext context, IQueryable query)
        {
            var cmd = context.GetCommand(query);
            return cmd.PreviewCommandText(false);
        }

        /// <summary>
        /// Returns a string representation of the <see cref="DbCommand.CommandText"/> along with <see cref="DbCommand.Parameters"/> if present.
        /// </summary>
        /// <param name="cmd">The <see cref="DbCommand"/> to analyze.</param>
        /// <param name="forTransactSQL">Whether or not the text should be formatted as 'logging' similiar to LINQ to SQL output, or in valid Transact SQL syntax ready for use with a 'query analyzer' type tool.</param>
        /// <returns>Returns a string representation of the <see cref="DbCommand.CommandText"/> along with <see cref="DbCommand.Parameters"/> if present.</returns>
        /// <remarks>This method is useful for debugging purposes or when used in other utilities such as LINQPad.</remarks>
        private static string PreviewCommandText(this DbCommand cmd, bool forTransactSQL)
        {
            var output = new StringBuilder();

            if (!forTransactSQL) output.AppendLine(cmd.CommandText);

            foreach (DbParameter parameter in cmd.Parameters)
            {
                int num = 0;
                int num2 = 0;
                PropertyInfo property = parameter.GetType().GetProperty("Precision");
                if (property != null)
                {
                    num = (int)Convert.ChangeType(property.GetValue(parameter, null), typeof(int), CultureInfo.InvariantCulture);
                }
                PropertyInfo info2 = parameter.GetType().GetProperty("Scale");
                if (info2 != null)
                {
                    num2 = (int)Convert.ChangeType(info2.GetValue(parameter, null), typeof(int), CultureInfo.InvariantCulture);
                }
                SqlParameter parameter2 = parameter as SqlParameter;

                if (forTransactSQL)
                {
                    output.AppendFormat("DECLARE {0} {1}{2}; SET {0} = {3}\r\n",
                        new object[] {
                                                                parameter.ParameterName,
                                                                ( parameter2 == null ) ? parameter.DbType.ToString() : parameter2.SqlDbType.ToString(),
                                                                ( parameter.Size > 0 ) ? "( " + parameter.Size.ToString( CultureInfo.CurrentCulture ) + " )" : "",
                                                                GetParameterTransactValue( parameter, parameter2 ) });
                }
                else
                {
                    output.AppendFormat("-- {0}: {1} {2} (Size = {3}; Prec = {4}; Scale = {5}) [{6}]\r\n", new object[] { parameter.ParameterName, parameter.Direction, (parameter2 == null) ? parameter.DbType.ToString() : parameter2.SqlDbType.ToString(), parameter.Size.ToString(CultureInfo.CurrentCulture), num, num2, (parameter2 == null) ? parameter.Value : parameter2.SqlValue });
                }
            }

            if (forTransactSQL) output.Append("\r\n" + cmd.CommandText);

            return output.ToString();
        }

        private static string GetParameterTransactValue(DbParameter parameter, SqlParameter parameter2)
        {
            if (parameter2 == null) return parameter.Value.ToString(); // Not going to deal with NON SQL parameters.

            switch (parameter2.SqlDbType)
            {
                case SqlDbType.Char:
                case SqlDbType.Date:
                case SqlDbType.DateTime:
                case SqlDbType.DateTime2:
                case SqlDbType.NChar:
                case SqlDbType.NText:
                case SqlDbType.NVarChar:
                case SqlDbType.SmallDateTime:
                case SqlDbType.Text:
                case SqlDbType.VarChar:
                case SqlDbType.UniqueIdentifier:
                    return string.Format("'{0}'", parameter2.SqlValue);

                default:
                    return parameter2.SqlValue.ToString();
            }
        }

        private static DbCommand GetDeleteBatchCommand<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities) where TEntity : class
        {
            var deleteCommand = table.Context.GetCommand(entities);
            deleteCommand.CommandText = string.Format("DELETE {0}\r\n", table.GetDbName()) + GetBatchJoinQuery<TEntity>(table, entities);
            return deleteCommand;
        }

        private static DbCommand GetUpdateBatchCommand<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities, Expression<Func<TEntity, TEntity>> evaluator) where TEntity : class
        {
            var updateCommand = table.Context.GetCommand(entities);

            var setSB = new StringBuilder();
            int memberInitCount = 1;

            // Process the MemberInitExpression (there should only be one in the evaluator Lambda) to convert the expression tree
            // into a valid DbCommand.  The Visit<> method will only process expressions of type MemberInitExpression and requires
            // that a MemberInitExpression be returned - in our case we'll return the same one we are passed since we are building
            // a DbCommand and not 'really using' the evaluator Lambda.
            evaluator.Visit<MemberInitExpression>(delegate(MemberInitExpression expression)
            {
                if (memberInitCount > 1)
                {
                    throw new NotImplementedException("Currently only one MemberInitExpression is allowed for the evaluator parameter.");
                }
                memberInitCount++;

                setSB.Append(GetDbSetStatement<TEntity>(expression, table, updateCommand));

                return expression; // just return passed in expression to keep 'visitor' happy.
            });

            // Complete the command text by concatenating bits together.
            updateCommand.CommandText = string.Format("UPDATE {0}\r\n{1}\r\n\r\n{2}",
                                                            table.GetDbName(),                                                                  // Database table name
                                                            setSB.ToString(),                                                                   // SET fld = {}, fld2 = {}, ...
                                                            GetBatchJoinQuery<TEntity>(table, entities));       // Subquery join created from entities command text

            if (updateCommand.CommandText.IndexOf("[arg0]") >= 0 || updateCommand.CommandText.IndexOf("NULL AS [EMPTY]") >= 0)
            {
                // TODO (Chris): Probably a better way to determine this by using an visitor on the expression before the
                //                               var selectExpression = Expression.Call... method call (search for that) and see which funcitons
                //                               are being used and determine if supported by LINQ to SQL
                throw new NotSupportedException(string.Format("The evaluator Expression<Func<{0},{0}>> has processing that needs to be performed once the query is returned (i.e. string.Format()) and therefore can not be used during batch updating.", table.GetType()));
            }

            return updateCommand;
        }

        private static string GetDbSetStatement<TEntity>(MemberInitExpression memberInitExpression, Table<TEntity> table, DbCommand updateCommand) where TEntity : class
        {
            var entityType = typeof(TEntity);

            if (memberInitExpression.Type != entityType)
            {
                throw new NotImplementedException(string.Format("The MemberInitExpression is intializing a class of the incorrect type '{0}' and it should be '{1}'.", memberInitExpression.Type, entityType));
            }

            var setSB = new StringBuilder();

            var tableName = table.GetDbName();
            var metaTable = table.Context.Mapping.GetTable(entityType);
            // Used to look up actual field names when MemberAssignment is a constant,
            // need both the Name (matches the property name on LINQ object) and the
            // MappedName (db field name).
            var dbCols = from mdm in metaTable.RowType.DataMembers
                         select new { mdm.MappedName, mdm.Name };

            // Walk all the expression bindings and generate SQL 'commands' from them.  Each binding represents a property assignment
            // on the TEntity object initializer Lambda expression.
            foreach (var binding in memberInitExpression.Bindings)
            {
                var assignment = binding as MemberAssignment;

                if (binding == null)
                {
                    throw new NotImplementedException("All bindings inside the MemberInitExpression are expected to be of type MemberAssignment.");
                }

                // TODO (Document): What is this doing?  I know it's grabbing existing parameter to pass into Expression.Call() but explain 'why'
                //                                      I assume it has something to do with fact we can't just access the parameters of assignment.Expression?
                //                                      Also, any concerns of whether or not if there are two params of type entity type?
                ParameterExpression entityParam = null;
                assignment.Expression.Visit<ParameterExpression>(delegate(ParameterExpression p) { if (p.Type == entityType) entityParam = p; return p; });

                // Get the real database field name.  binding.Member.Name is the 'property' name of the LINQ object
                // so I match that to the Name property of the table mapping DataMembers.
                string name = binding.Member.Name;
                var dbCol = (from c in dbCols
                             where c.Name == name
                             select c).FirstOrDefault();

                if (dbCol == null)
                {
                    throw new ArgumentOutOfRangeException(name, string.Format("The corresponding field on the {0} table could not be found.", tableName));
                }

                // If entityParam is NULL, then no references to other columns on the TEntity row and need to eval 'constant' value...
                if (entityParam == null)
                {
                    // Compile and invoke the assignment expression to obtain the contant value to add as a parameter.
                    var constant = Expression.Lambda(assignment.Expression, null).Compile().DynamicInvoke();

                    // use the MappedName from the table mapping DataMembers - that is field name in DB table.
                    if (constant == null)
                    {
                        setSB.AppendFormat("[{0}] = null, ", dbCol.MappedName);
                    }
                    else
                    {
                        // Add new parameter with massaged name to avoid clashes.
                        setSB.AppendFormat("[{0}] = @p{1}, ", dbCol.MappedName, updateCommand.Parameters.Count);
                        updateCommand.Parameters.Add(new SqlParameter(string.Format("@p{0}", updateCommand.Parameters.Count), constant));
                    }
                }
                else
                {
                    // TODO (Documentation): Explain what we are doing here again, I remember you telling me why we have to call but I can't remember now.
                    // Wny are we calling Expression.Call and what are we passing it?  Below comments are just 'made up' and probably wrong.

                    // Create a MethodCallExpression which represents a 'simple' select of *only* the assignment part (right hand operator) of
                    // of the MemberInitExpression.MemberAssignment so that we can let the Linq Provider do all the 'sql syntax' generation for
                    // us.
                    //
                    // For Example: TEntity.Property1 = TEntity.Property1 + " Hello"
                    // This selectExpression will be only dealing with TEntity.Property1 + " Hello"
                    var selectExpression = Expression.Call(
                                                typeof(Queryable),
                                                "Select",
                                                new Type[] { entityType, assignment.Expression.Type },

                    // TODO (Documentation): How do we know there are only 'two' parameters?  And what is Expression.Lambda
                        //                                               doing?  I assume it's returning a type of assignment.Expression.Type to match above?

                                                Expression.Constant(table),
                                                Expression.Lambda(assignment.Expression, entityParam));

                    setSB.AppendFormat("[{0}] = {1}, ",
                                            dbCol.MappedName,
                                            GetDbSetAssignment(table, selectExpression, updateCommand, name));
                }
            }

            var setStatements = setSB.ToString();
            return "SET " + setStatements.Substring(0, setStatements.Length - 2); // remove ', '
        }

        /// <summary>
        /// Some LINQ Query syntax is invalid because SQL (or whomever the provider is) can not translate it to its native language.  
        /// DataContext.GetCommand() does not detect this, only IProvider.Execute or IProvider.Compile call the necessary code to
        /// check this.  This function invokes the IProvider.Compile to make sure the provider can translate the expression.
        /// </summary>
        /// <remarks>
        /// An example of a LINQ query that previously 'worked' in the *Batch methods but needs to throw an exception is something
        /// like the following:
        ///
        /// var pay =
        ///             from h in HistoryData
        ///             where h.his.Groups.gName == "Ochsner" && h.hisType == "pay"
        ///             select h;
        ///            
        /// HistoryData.UpdateBatchPreview( pay, h => new HistoryData { hisIndex = ( int.Parse( h.hisIndex ) - 1 ).ToString() } ).Dump();
        ///
        /// The int.Parse is not valid and needs to throw an exception like:
        ///
        ///             Could not translate expression '(Parse(p.hisIndex) - 1).ToString()' into SQL and could not treat it as a local expression.
        ///            
        ///     Unfortunately, the IProvider.Compile is internal and I need to use Reflection to call it (ugh).  I've several e-mails sent into
        ///     MS LINQ team members and am waiting for a response and will correct/improve code as soon as possible.
        /// </remarks>
        private static void ValidateExpression(ITable table, Expression expression)
        {
            var context = table.Context;
            PropertyInfo providerProperty = context.GetType().GetProperty("Provider", BindingFlags.Instance | BindingFlags.NonPublic);
            var provider = providerProperty.GetValue(context, null);
            var compileMI = provider.GetType().GetMethod("System.Data.Linq.Provider.IProvider.Compile", BindingFlags.Instance | BindingFlags.NonPublic);

            // Simply compile the expression to see if it will work.
            compileMI.Invoke(provider, new object[] { expression });
        }

        private static string GetDbSetAssignment(ITable table, MethodCallExpression selectExpression, DbCommand updateCommand, string bindingName)
        {
            ValidateExpression(table, selectExpression);

            // Convert the selectExpression into an IQueryable query so that I can get the CommandText
            var selectQuery = (table as IQueryable).Provider.CreateQuery(selectExpression);

            // Get the DbCommand so I can grab relavent parts of CommandText to construct a field
            // assignment and based on the 'current TEntity row'.  Additionally need to massage parameter
            // names from temporary command when adding to the final update command.
            var selectCmd = table.Context.GetCommand(selectQuery);
            var selectStmt = selectCmd.CommandText;
            selectStmt = selectStmt.Substring(7,                                                                        // Remove 'SELECT ' from front ( 7 )
                                        selectStmt.IndexOf("\r\nFROM ") - 7)            // Return only the selection field expression
                                    .Replace("[t0].", "")                                                       // Remove table alias from the select
                                    .Replace(" AS [value]", "")                                 // If the select is not a direct field (constant or expression), remove the field alias
                                    .Replace("@p", "@p" + bindingName);                 // Replace parameter name so doesn't conflict with existing ones.

            foreach (var selectParam in selectCmd.Parameters.Cast<DbParameter>())
            {
                var paramName = string.Format("@p{0}", updateCommand.Parameters.Count);

                // DataContext.ExecuteCommand ultimately just takes a object array of parameters and names them p0-N.  
                // So I need to now do replaces on the massaged value to get it in proper format.
                selectStmt = selectStmt.Replace(
                                    selectParam.ParameterName.Replace("@p", "@p" + bindingName),
                                    paramName);

                updateCommand.Parameters.Add(new SqlParameter(paramName, selectParam.Value));
            }

            return selectStmt;
        }

        private static string GetBatchJoinQuery<TEntity>(Table<TEntity> table, IQueryable<TEntity> entities) where TEntity : class
        {
            var metaTable = table.Context.Mapping.GetTable(typeof(TEntity));

            var keys = from mdm in metaTable.RowType.DataMembers
                       where mdm.IsPrimaryKey
                       select new { mdm.MappedName };

            var joinSB = new StringBuilder();
            var subSelectSB = new StringBuilder();

            foreach (var key in keys)
            {
                joinSB.AppendFormat("j0.[{0}] = j1.[{0}] AND ", key.MappedName);
                // For now, always assuming table is aliased as t0.  Should probably improve at some point.
                // Just writing a smaller sub-select so it doesn't get all the columns of data, but instead
                // only the primary key fields used for joining.
                subSelectSB.AppendFormat("[t0].[{0}], ", key.MappedName);
            }

            var selectCommand = table.Context.GetCommand(entities);
            var select = selectCommand.CommandText;

            var join = joinSB.ToString();

            if (join == "")
            {
                throw new MissingPrimaryKeyException(string.Format("{0} does not have a primary key defined.  Batch updating/deleting can not be used for tables without a primary key.", metaTable.TableName));
            }

            join = join.Substring(0, join.Length - 5);                                                                                  // Remove last ' AND '
            #region - Better ExpressionTree Handling Needed -
            /*
                       
                        Below is a sample query where the let statement was used to simply the 'where clause'.  However, it produced an extra level
                        in the query.
                         
                        var manage =
                                from u in User
                                join g in Groups on u.User_Group_id equals g.gKey into groups
                                from g in groups.DefaultIfEmpty()
                                let correctGroup = groupsToManage.Contains( g.gName ) || ( groupsToManage.Contains( "_GLOBAL" ) && g.gKey == null )
                                where correctGroup && ( users.Contains( u.User_Authenticate_id ) || userEmails.Contains( u.User_Email ) ) || userKeys.Contains( u.User_id )
                                select u;
                         
                        Produces this SQL:
                        SELECT [t2].[User_id] AS [uKey], [t2].[User_Authenticate_id] AS [uAuthID], [t2].[User_Email] AS [uEmail], [t2].[User_Pin] AS [uPin], [t2].[User_Active] AS [uActive], [t2].[uAdminAuthID], [t2].[uFailureCount]
                        FROM (
                                SELECT [t0].[User_id], [t0].[User_Authenticate_id], [t0].[User_Email], [t0].[User_Pin], [t0].[User_Active], [t0].[uFailureCount], [t0].[uAdminAuthID],
                                        (CASE
                                                WHEN [t1].[gName] IN (@p0) THEN 1
                                                WHEN NOT ([t1].[gName] IN (@p0)) THEN 0
                                                ELSE NULL
                                         END) AS [value]
                                FROM [User] AS [t0]
                                LEFT OUTER JOIN [Groups] AS [t1] ON [t0].[User_Group_id] = ([t1].[gKey])
                                ) AS [t2]
                        WHERE (([t2].[value] = 1) AND (([t2].[User_Authenticate_id] IN (@p1)) OR ([t2].[User_Email] IN (@p2)))) OR ([t2].[User_id] IN (@p3))                    
                       
                        If I put the entire where in one line...
                        where   ( groupsToManage.Contains( g.gName ) || ( groupsToManage.Contains( "_GLOBAL" ) && g.gKey == null ) ) &&
                                        ( users.Contains( u.User_Authenticate_id ) || userEmails.Contains( u.User_Email ) ) || userKeys.Contains ( u.User_id )

                        I get this SQL:
                        SELECT [t0].[User_id] AS [uKey], [t0].[User_Authenticate_id] AS [uAuthID], [t0].[User_Email] AS [uEmail], [t0].[User_Pin] AS [uPin], [t0].[User_Active] AS [uActive], [t0].[uAdminAuthID], [t0].[uFailureCount]
                        FROM [User] AS [t0]
                        LEFT OUTER JOIN [Groups] AS [t1] ON [t0].[User_Group_id] = ([t1].[gKey])
                        WHERE (([t1].[gName] IN (@p0)) AND (([t0].[User_Authenticate_id] IN (@p1)) OR ([t0].[User_Email] IN (@p2)))) OR ([t0].[User_id] IN (@p3))                      
                       
                        The second 'cleaner' SQL worked with my original 'string parsing' of simply looking for [t0] and stripping everything before it
                        to get rid of the SELECT and any 'TOP' clause if present.  But the first SQL introduced a layer which caused [t2] to be used.  So
                        I have to do a bit different string parsing.  There is probably a more efficient way to examine the ExpressionTree and figure out
                        if something like this is going to happen.  I will explore it later.
                        */
            #endregion
            var endSelect = select.IndexOf("[t");         
            var selectClause = select.Substring(0, endSelect);
            var selectTableNameStart = endSelect + 1;                                                                                           // Get the table name LINQ to SQL used in query generation
            var selectTableName = select.Substring(selectTableNameStart,                                                        // because I have to replace [t0] with it in the subSelectSB
                                        select.IndexOf("]", selectTableNameStart) - (selectTableNameStart));

            // TODO (Chris): I think instead of searching for ORDER BY in the entire select statement, I should examine the ExpressionTree and see
            // if the *outer* select (in case there are nested subselects) has an orderby clause applied to it.
            var needsTopClause = selectClause.IndexOf(" TOP ") < 0 && select.IndexOf("\r\nORDER BY ") > 0;

            var subSelect = selectClause
                                + (needsTopClause ? "TOP 100 PERCENT " : "")                                                    // If order by in original select without TOP clause, need TOP
                                + subSelectSB.ToString()                                                                                                // Append just the primary keys.
                                             .Replace("[t0]", string.Format("[{0}]", selectTableName));
            subSelect = subSelect.Substring(0, subSelect.Length - 2);                                                                   // Remove last ', '

            subSelect += select.Substring(select.IndexOf("\r\nFROM ")); // Create a sub SELECT that *only* includes the primary key fields

            var batchJoin = String.Format("FROM {0} AS j0 INNER JOIN (\r\n\r\n{1}\r\n\r\n) AS j1 ON ({2})\r\n", table.GetDbName(), subSelect, join);
            return batchJoin;
        }

        private static string GetDbName<TEntity>(this Table<TEntity> table) where TEntity : class
        {
            var entityType = typeof(TEntity);
            var metaTable = table.Context.Mapping.GetTable(entityType);
            var tableName = metaTable.TableName;

            if (!tableName.StartsWith("["))
            {
                string[] parts = tableName.Split('.');
                tableName = string.Format("[{0}]", string.Join("].[", parts));
            }

            return tableName;
        }

        private static string GetLog(this DataContext context)
        {
            PropertyInfo providerProperty = context.GetType().GetProperty("Provider", BindingFlags.Instance | BindingFlags.NonPublic);
            var provider = providerProperty.GetValue(context, null);
            Type providerType = provider.GetType();

            PropertyInfo modeProperty = providerType.GetProperty("Mode", BindingFlags.Instance | BindingFlags.NonPublic);
            FieldInfo servicesField = providerType.GetField("services", BindingFlags.Instance | BindingFlags.NonPublic);
            object services = servicesField != null ? servicesField.GetValue(provider) : null;
            PropertyInfo modelProperty = services != null ? services.GetType().GetProperty("Model", BindingFlags.Instance | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.GetProperty) : null;

            return string.Format("-- Context: {0}({1}) Model: {2} Build: {3}\r\n",
                            providerType.Name,
                            modeProperty != null ? modeProperty.GetValue(provider, null) : "unknown",
                            modelProperty != null ? modelProperty.GetValue(services, null).GetType().Name : "unknown",
                            "3.5.21022.8");
        }

        /// <summary>
        /// Returns a list of changed items inside the Context before being applied to the data store.
        /// </summary>
        /// <param name="context">The DataContext to interogate for pending changes.</param>
        /// <returns>Returns a list of changed items inside the Context before being applied to the data store.</returns>
        /// <remarks>Based on Ryan Haney's code at http://dotnetslackers.com/articles/csharp/GettingChangedEntitiesFromLINQToSQLDataContext.aspx.  Note that this code relies on reflection of private fields and members.</remarks>
        public static List<ChangedItems<TEntity>> GetChangedItems<TEntity>(this DataContext context)
        {
            // create a dictionary of type TItem for return to caller
            List<ChangedItems<TEntity>> changedItems = new List<ChangedItems<TEntity>>();

            PropertyInfo providerProperty = context.GetType().GetProperty("Provider", BindingFlags.Instance | BindingFlags.NonPublic);
            var provider = providerProperty.GetValue(context, null);
            Type providerType = provider.GetType();

            // use reflection to get changed items from data context
            object services = providerType.GetField("services",
              BindingFlags.NonPublic |
              BindingFlags.Instance |
              BindingFlags.GetField).GetValue(provider);

            object tracker = services.GetType().GetField("tracker",
              BindingFlags.NonPublic |
              BindingFlags.Instance |
              BindingFlags.GetField).GetValue(services);

            System.Collections.IDictionary trackerItems =
              (System.Collections.IDictionary)tracker.GetType().GetField("items",
              BindingFlags.NonPublic |
              BindingFlags.Instance |
              BindingFlags.GetField).GetValue(tracker);

            // iterate through each item in context, adding
            // only those that are of type TItem to the changedItems dictionary
            foreach (System.Collections.DictionaryEntry entry in trackerItems)
            {
                object original = entry.Value.GetType().GetField("original",
                                  BindingFlags.NonPublic |
                                  BindingFlags.Instance |
                                  BindingFlags.GetField).GetValue(entry.Value);

                if (entry.Key is TEntity && original is TEntity)
                {
                    changedItems.Add(
                      new ChangedItems<TEntity>((TEntity)entry.Key, (TEntity)original)
                    );
                }
            }
            return changedItems;
        }

        public static List<string> SQL_Modificar<TEntity>(this Table<TEntity> table, IQueryable<TEntity> entities) where TEntity : class
        {
            int limite = 2000;
            int total = (entities.Count() / limite);
            int total_lt = entities.Count();

            List<string> lt = new List<string>();
             
            var type = table.Context.Mapping.GetMetaType(typeof(TEntity));

            var PK = (from m in type.DataMembers
                      where m.IsPrimaryKey
                      select m).Single();            
             
            if(PK== null)
                throw new MissingPrimaryKeyException(string.Format("{0} Tabla sin PK, ne cesita especificar alguna para efectos de update.", table.GetDbName()));

            string pk_name = PK.Name;
            
            var l_data_member_info = (from x in type.DataMembers
                                    select new {x.Name, x.CanBeNull}).ToList();

            for (int i = 1; i <= (total + 1); i++)
            {
                StringBuilder sb = new StringBuilder();                
                var lt_sql = entities.Skip((i - 1) * limite).Take(limite);
                DataTable dt = lt_sql.ToList().ToDataTable();
                               
                var l_dt_elemnts = (from DataRow x in dt.Rows
                                    from DataColumn y in x.Table.Columns
                                    from z in l_data_member_info
                                    where y.ColumnName.Equals(z.Name)
                                    select new
                                    {
                                        pk_id = x[pk_name],
                                        columna = y.ColumnName,
                                        tipo = y.DataType,
                                        isNulleable = z.CanBeNull,
                                        valor = x[y.ColumnName]                                        
                                    })
                                    .GroupBy(p => p.pk_id)
                                    .Select(p => new
                                    {
                                        pk_id = p.Key,
                                        l_elements = p.Select(o => new
                                        {
                                            o.columna,
                                            o.valor,
                                            o.tipo,
                                            o.isNulleable
                                        }).ToList()
                                    }).AsParallel().ToList();

                foreach (var l_element in l_dt_elemnts)
                {                    
                    string str_update_local = l_element.l_elements.Where(p => p.columna != pk_name).Select(p => new { p.columna, valor_final = Normalizar_Campo(p.tipo, p.valor), p.isNulleable, p.tipo }).ToList().Select(p => new { x = string.Format("[{0}] = {1}", p.columna, p.valor_final != null ? string.Format("''{0}''", p.valor_final) : (p.isNulleable ? "null" : CrearInstancia(p.tipo).ToString())) }).ToList().ToStringSeparator(p => p.x);
                    string str_where = string.Format("WHERE [{0}] = {1}'", pk_name, l_element.pk_id);

                    sb.AppendLine(string.Format("exec sp_executesql N'UPDATE {0} SET {1} {2}", table.GetDbName(), str_update_local, str_where));
                    sb.AppendLine(string.Empty);
                }

                if ((lt_sql.Count() > 0))
                    lt.Add(sb.ToString());

            }

            return lt;
        }
        
        private static dynamic CrearInstancia(Type _tipo)
        {
            try
            {
                string typeName = typeof(Counter<>).AssemblyQualifiedName;
                Type t = Type.GetType(typeName);                
                
                dynamic item = Activator.CreateInstance(t.MakeGenericType(_tipo));
                return item.Value;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        private static dynamic Normalizar_Campo(Type _tipo_campo, object _valor)
        {
            try
            {                
                dynamic result = null;
                _tipo_campo = _tipo_campo.IsGenericType ? _tipo_campo.GetGenericTypeDefinition() == typeof(Nullable<>) ? Nullable.GetUnderlyingType(_tipo_campo) : _tipo_campo : _tipo_campo;
                bool isNumericValue = false;

                switch (Type.GetTypeCode(_tipo_campo))
                {
                    case TypeCode.Int32:
                        {
                            int num = 0;
                            bool _r_parse = Int32.TryParse(_valor.ToString(), out num);
                            if (_r_parse) { result = num; isNumericValue = true; }
                            break;
                        }
                    case TypeCode.Decimal:
                        {
                            decimal num = 0;
                            bool _r_parse = Decimal.TryParse(_valor.ToString(), out num);
                            if (_r_parse) { result = num; isNumericValue = true; }
                            else { result = Normalizar_Campo(typeof(double), _valor); }
                            break;
                        }
                    case TypeCode.Double:
                        {
                            double num = 0;
                            bool _r_parse = Double.TryParse(_valor.ToString(), out num);
                            if (_r_parse) { result = num; isNumericValue = true; }
                            break;
                        }
                    case TypeCode.DateTime:
                        {
                            DateTime num = new DateTime();
                            bool _r_parse = DateTime.TryParse(_valor.ToString(), out num);
                            if (_r_parse) { result = num; }
                            else
                            {
                                int fechakey_id;
                                bool r_parse_int = Int32.TryParse(_valor.ToString(), out fechakey_id);
                                if (r_parse_int)
                                {
                                    if (fechakey_id.ToString().Length == 8)
                                    {
                                        num = new DateTime(int.Parse(fechakey_id.ToString().Substring(0, 4)), int.Parse(fechakey_id.ToString().Substring(4, 2)), int.Parse(fechakey_id.ToString().Substring(6, 2)));
                                        result = num;
                                    }
                                }
                            }
                            break;
                        }
                    case TypeCode.String:
                        {
                            string str = _valor.ToString();
                            if (string.IsNullOrEmpty(str)) result = string.Empty;
                            if (string.IsNullOrWhiteSpace(str)) result = string.Empty;                            
                            break;
                        }
                    case TypeCode.Boolean:
                        {
                            int num = 0;
                            bool _r_parse = Int32.TryParse(_valor.ToString(), out num);
                            if (_r_parse) { result = num == 0 ? false : true; }
                            else 
                            {
                                bool res = false;
                                bool _r_parse_str = bool.TryParse(_valor.ToString(), out res);
                                if (_r_parse_str) { result = res; }
                            }
                            break;
                        }
                    default:
                        {
                            result = _valor;
                            break;
                        }
                        
                }

                if (isNumericValue)
                {
                    NumberFormatInfo nfi = new CultureInfo("es-CL", false).NumberFormat;
                    nfi.NumberDecimalSeparator = ".";

                    result = Convert.ToString(result, nfi);
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static T GetProperty<T>(object o, string propertyName)
        {
            var theProperty = o.GetType().GetProperties().FirstOrDefault(p => p.Name == propertyName);

            if (theProperty == null)
                throw new ArgumentException("object does not have an " + propertyName + " property", "o");

            if (theProperty.PropertyType.FullName != typeof(T).FullName)
                throw new ArgumentException("object has an Id property, but it is not of type " + typeof(T).FullName, "o");

            return (T)theProperty.GetValue(o, new object[] { });
        }

    }

    public class Counter<T>
    {
        public T Value { get; set; }
    }

    public class ChangedItems<TEntity>
    {
        public ChangedItems(TEntity Current, TEntity Original)
        {
            this.Current = Current;
            this.Original = Original;
        }
        public TEntity Current { get; set; }
        public TEntity Original { get; set; }
    }
    #endregion

    public static class EnumerableExtension
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration"></param>
        /// <param name="action"></param>
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {

            foreach (T item in enumeration)
            {
                action(item);
            }
        }

        /// <summary>
        /// Convert  list to Data Table
        /// </summary>
        /// <typeparam name="T">Target Class</typeparam>
        /// <param name="varlist">list you want to convert it to Data Table</param>
        /// <param name="fn">Delegate Function to Create Row</param>
        /// <returns>Data Table That Represent List data</returns>
        public static DataTable ToADOTable<T>(this IEnumerable<T> varlist, CreateRowDelegate<T> fn)
        {
            DataTable toReturn = new DataTable();

            // Could add a check to verify that there is an element 0
            T TopRec = varlist.ElementAtOrDefault(0);

            if (TopRec == null)
                return toReturn;

            // Use reflection to get property names, to create table
            // column names

            PropertyInfo[] oProps = ((Type)TopRec.GetType()).GetProperties();

            foreach (PropertyInfo pi in oProps)
            {
                Type pt = pi.PropertyType;
                if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                    pt = Nullable.GetUnderlyingType(pt);
                toReturn.Columns.Add(pi.Name, pt);
            }

            foreach (T rec in varlist)
            {
                DataRow dr = toReturn.NewRow();
                foreach (PropertyInfo pi in oProps)
                {
                    object o = pi.GetValue(rec, null);
                    if (o == null)
                        dr[pi.Name] = DBNull.Value;
                    else
                        dr[pi.Name] = o;
                }
                toReturn.Rows.Add(dr);
            }

            return toReturn;
        }

        /// <summary>
        /// Convert  list to Data Table
        /// </summary>
        /// <typeparam name="T">Target Class</typeparam>
        /// <param name="varlist">list you want to convert it to Data Table</param>
        /// <returns>Data Table That Represent List data</returns>
        public static DataTable ToADOTable<T>(this IEnumerable<T> varlist)
        {
            DataTable toReturn = new DataTable();

            // Could add a check to verify that there is an element 0
            T TopRec = varlist.ElementAtOrDefault(0);

            if (TopRec == null)
                return toReturn;

            // Use reflection to get property names, to create table
            // column names

            PropertyInfo[] oProps = ((Type)TopRec.GetType()).GetProperties();

            foreach (PropertyInfo pi in oProps)
            {
                Type pt = pi.PropertyType;
                if (pt.IsGenericType && pt.GetGenericTypeDefinition() == typeof(Nullable<>))
                    pt = Nullable.GetUnderlyingType(pt);
                toReturn.Columns.Add(pi.Name, pt);
            }

            foreach (T rec in varlist)
            {
                DataRow dr = toReturn.NewRow();
                foreach (PropertyInfo pi in oProps)
                {
                    object o = pi.GetValue(rec, null);

                    if (o == null)
                        dr[pi.Name] = DBNull.Value;
                    else
                        dr[pi.Name] = o;
                }
                toReturn.Rows.Add(dr);
            }

            return toReturn;
        }
        
        /// <summary>
        /// Convert Data Table To List of Type T
        /// </summary>
        /// <typeparam name="T">Target Class to convert data table to List of T </typeparam>
        /// <param name="datatable">Data Table you want to convert it</param>
        /// <returns>List of Target Class</returns>
        public static List<T> ToList<T>(this DataTable datatable) where T : new()
        {
            List<T> Temp = new List<T>();
            try
            {
                List<string> columnsNames = new List<string>();
                foreach (DataColumn DataColumn in datatable.Columns)
                    columnsNames.Add(DataColumn.ColumnName);
                Temp = datatable.AsEnumerable().ToList().ConvertAll<T>(row => getObject<T>(row, columnsNames));
                return Temp;
            }
            catch { return Temp; }
        }

        public static T getObject<T>(DataRow row, List<string> columnsName) where T : new()
        {
            T obj = new T();
            try
            {
                string columnname = "";
                string value = "";
                PropertyInfo[] Properties; Properties = typeof(T).GetProperties();
                foreach (PropertyInfo objProperty in Properties)
                {
                    columnname = columnsName.Find(name => name.ToLower() == objProperty.Name.ToLower());
                    if (!string.IsNullOrEmpty(columnname))
                    {
                        value = row[columnname].ToString();
                        if (!string.IsNullOrEmpty(value))
                        {
                            if (Nullable.GetUnderlyingType(objProperty.PropertyType) != null)
                            {
                                value = row[columnname].ToString().Replace("$", "").Replace(",", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(Nullable.GetUnderlyingType(objProperty.PropertyType).ToString())), null);
                            }
                            else
                            {
                                value = row[columnname].ToString().Replace("%", "");
                                objProperty.SetValue(obj, Convert.ChangeType(value, Type.GetType(objProperty.PropertyType.ToString())), null);
                            }
                        }
                    }
                } return obj;
            }
            catch { return obj; }
        }

        public delegate object[] CreateRowDelegate<T>(T t);
    }

    #region Expressions    
    public static class ExpressionExtensions
    {
        public static Expression Visit<T>(
            this Expression exp,
            Func<T, Expression> visitor) where T : Expression
        {
            return ExpressionVisitor<T>.Visit(exp, visitor);
        }

        public static TExp Visit<T, TExp>(
            this TExp exp,
            Func<T, Expression> visitor)
            where T : Expression
            where TExp : Expression
        {
            return (TExp)ExpressionVisitor<T>.Visit(exp, visitor);
        }

        public static Expression<TDelegate> Visit<T, TDelegate>(
            this Expression<TDelegate> exp,
            Func<T, Expression> visitor) where T : Expression
        {
            return ExpressionVisitor<T>.Visit<TDelegate>(exp, visitor);
        }

        public static IQueryable<TSource> Visit<T, TSource>(
            this IQueryable<TSource> source,
            Func<T, Expression> visitor) where T : Expression
        {
            return source.Provider.CreateQuery<TSource>(ExpressionVisitor<T>.Visit(source.Expression, visitor));
        }
    }

    /// <summary>
    /// This class visits every Parameter expression in an expression tree and calls a delegate
    /// to optionally replace the parameter.  This is useful where two expression trees need to
    /// be merged (and they don't share the same ParameterExpressions).
    /// </summary>
    public class ExpressionVisitor<T> : ExpressionVisitor where T : Expression
    {
        Func<T, Expression> visitor;

        public ExpressionVisitor(Func<T, Expression> visitor)
        {
            this.visitor = visitor;
        }

        public static Expression Visit(
            Expression exp,
            Func<T, Expression> visitor)
        {
            return new ExpressionVisitor<T>(visitor).Visit(exp);
        }

        public static Expression<TDelegate> Visit<TDelegate>(
            Expression<TDelegate> exp,
            Func<T, Expression> visitor)
        {
            return (Expression<TDelegate>)new ExpressionVisitor<T>(visitor).Visit(exp);
        }

        protected override Expression Visit(Expression exp)
        {
            if (exp is T && visitor != null) exp = visitor((T)exp);

            return base.Visit(exp);
        }
    }

    /// <summary>
    /// Expression visitor
    /// (from http://blogs.msdn.com/mattwar/archive/2007/07/31/linq-building-an-iqueryable-provider-part-ii.aspx)
    /// </summary>
    public abstract class ExpressionVisitor
    {
        protected ExpressionVisitor()
        {
        }

        protected virtual Expression Visit(Expression exp)
        {
            if (exp == null)
                return exp;
            switch (exp.NodeType)
            {
                case ExpressionType.Negate:
                case ExpressionType.NegateChecked:
                case ExpressionType.Not:
                case ExpressionType.Convert:
                case ExpressionType.ConvertChecked:
                case ExpressionType.ArrayLength:
                case ExpressionType.Quote:
                case ExpressionType.TypeAs:
                    return this.VisitUnary((UnaryExpression)exp);
                case ExpressionType.Add:
                case ExpressionType.AddChecked:
                case ExpressionType.Subtract:
                case ExpressionType.SubtractChecked:
                case ExpressionType.Multiply:
                case ExpressionType.MultiplyChecked:
                case ExpressionType.Divide:
                case ExpressionType.Modulo:
                case ExpressionType.And:
                case ExpressionType.AndAlso:
                case ExpressionType.Or:
                case ExpressionType.OrElse:
                case ExpressionType.LessThan:
                case ExpressionType.LessThanOrEqual:
                case ExpressionType.GreaterThan:
                case ExpressionType.GreaterThanOrEqual:
                case ExpressionType.Equal:
                case ExpressionType.NotEqual:
                case ExpressionType.Coalesce:
                case ExpressionType.ArrayIndex:
                case ExpressionType.RightShift:
                case ExpressionType.LeftShift:
                case ExpressionType.ExclusiveOr:
                    return this.VisitBinary((BinaryExpression)exp);
                case ExpressionType.TypeIs:
                    return this.VisitTypeIs((TypeBinaryExpression)exp);
                case ExpressionType.Conditional:
                    return this.VisitConditional((ConditionalExpression)exp);
                case ExpressionType.Constant:
                    return this.VisitConstant((ConstantExpression)exp);
                case ExpressionType.Parameter:
                    return this.VisitParameter((ParameterExpression)exp);
                case ExpressionType.MemberAccess:
                    return this.VisitMemberAccess((MemberExpression)exp);
                case ExpressionType.Call:
                    return this.VisitMethodCall((MethodCallExpression)exp);
                case ExpressionType.Lambda:
                    return this.VisitLambda((LambdaExpression)exp);
                case ExpressionType.New:
                    return this.VisitNew((NewExpression)exp);
                case ExpressionType.NewArrayInit:
                case ExpressionType.NewArrayBounds:
                    return this.VisitNewArray((NewArrayExpression)exp);
                case ExpressionType.Invoke:
                    return this.VisitInvocation((InvocationExpression)exp);
                case ExpressionType.MemberInit:
                    return this.VisitMemberInit((MemberInitExpression)exp);
                case ExpressionType.ListInit:
                    return this.VisitListInit((ListInitExpression)exp);
                default:
                    throw new Exception(string.Format("Unhandled expression type: '{0}'", exp.NodeType));
            }
        }

        protected virtual MemberBinding VisitBinding(MemberBinding binding)
        {
            switch (binding.BindingType)
            {
                case MemberBindingType.Assignment:
                    return this.VisitMemberAssignment((MemberAssignment)binding);
                case MemberBindingType.MemberBinding:
                    return this.VisitMemberMemberBinding((MemberMemberBinding)binding);
                case MemberBindingType.ListBinding:
                    return this.VisitMemberListBinding((MemberListBinding)binding);
                default:
                    throw new Exception(string.Format("Unhandled binding type '{0}'", binding.BindingType));
            }
        }

        protected virtual ElementInit VisitElementInitializer(ElementInit initializer)
        {
            ReadOnlyCollection<Expression> arguments = this.VisitExpressionList(initializer.Arguments);
            if (arguments != initializer.Arguments)
            {
                return Expression.ElementInit(initializer.AddMethod, arguments);
            }
            return initializer;
        }

        protected virtual Expression VisitUnary(UnaryExpression u)
        {
            Expression operand = this.Visit(u.Operand);
            if (operand != u.Operand)
            {
                return Expression.MakeUnary(u.NodeType, operand, u.Type, u.Method);
            }
            return u;
        }

        protected virtual Expression VisitBinary(BinaryExpression b)
        {
            Expression left = this.Visit(b.Left);
            Expression right = this.Visit(b.Right);
            Expression conversion = this.Visit(b.Conversion);
            if (left != b.Left || right != b.Right || conversion != b.Conversion)
            {
                if (b.NodeType == ExpressionType.Coalesce && b.Conversion != null)
                    return Expression.Coalesce(left, right, conversion as LambdaExpression);
                else
                    return Expression.MakeBinary(b.NodeType, left, right, b.IsLiftedToNull, b.Method);
            }
            return b;
        }

        protected virtual Expression VisitTypeIs(TypeBinaryExpression b)
        {
            Expression expr = this.Visit(b.Expression);
            if (expr != b.Expression)
            {
                return Expression.TypeIs(expr, b.TypeOperand);
            }
            return b;
        }

        protected virtual Expression VisitConstant(ConstantExpression c)
        {
            return c;
        }

        protected virtual Expression VisitConditional(ConditionalExpression c)
        {
            Expression test = this.Visit(c.Test);
            Expression ifTrue = this.Visit(c.IfTrue);
            Expression ifFalse = this.Visit(c.IfFalse);
            if (test != c.Test || ifTrue != c.IfTrue || ifFalse != c.IfFalse)
            {
                return Expression.Condition(test, ifTrue, ifFalse);
            }
            return c;
        }

        protected virtual Expression VisitParameter(ParameterExpression p)
        {
            return p;
        }

        protected virtual Expression VisitMemberAccess(MemberExpression m)
        {
            Expression exp = this.Visit(m.Expression);
            if (exp != m.Expression)
            {
                return Expression.MakeMemberAccess(exp, m.Member);
            }
            return m;
        }

        protected virtual Expression VisitMethodCall(MethodCallExpression m)
        {
            Expression obj = this.Visit(m.Object);
            IEnumerable<Expression> args = this.VisitExpressionList(m.Arguments);
            if (obj != m.Object || args != m.Arguments)
            {
                return Expression.Call(obj, m.Method, args);
            }
            return m;
        }

        protected virtual ReadOnlyCollection<Expression> VisitExpressionList(ReadOnlyCollection<Expression> original)
        {
            List<Expression> list = null;
            for (int i = 0, n = original.Count; i < n; i++)
            {
                Expression p = this.Visit(original[i]);
                if (list != null)
                {
                    list.Add(p);
                }
                else if (p != original[i])
                {
                    list = new List<Expression>(n);
                    for (int j = 0; j < i; j++)
                    {
                        list.Add(original[j]);
                    }
                    list.Add(p);
                }
            }
            if (list != null)
            {
                return list.AsReadOnly();
            }
            return original;
        }

        protected virtual MemberAssignment VisitMemberAssignment(MemberAssignment assignment)
        {
            Expression e = this.Visit(assignment.Expression);
            if (e != assignment.Expression)
            {
                return Expression.Bind(assignment.Member, e);
            }
            return assignment;
        }

        protected virtual MemberMemberBinding VisitMemberMemberBinding(MemberMemberBinding binding)
        {
            IEnumerable<MemberBinding> bindings = this.VisitBindingList(binding.Bindings);
            if (bindings != binding.Bindings)
            {
                return Expression.MemberBind(binding.Member, bindings);
            }
            return binding;
        }

        protected virtual MemberListBinding VisitMemberListBinding(MemberListBinding binding)
        {
            IEnumerable<ElementInit> initializers = this.VisitElementInitializerList(binding.Initializers);
            if (initializers != binding.Initializers)
            {
                return Expression.ListBind(binding.Member, initializers);
            }
            return binding;
        }

        protected virtual IEnumerable<MemberBinding> VisitBindingList(ReadOnlyCollection<MemberBinding> original)
        {
            List<MemberBinding> list = null;
            for (int i = 0, n = original.Count; i < n; i++)
            {
                MemberBinding b = this.VisitBinding(original[i]);
                if (list != null)
                {
                    list.Add(b);
                }
                else if (b != original[i])
                {
                    list = new List<MemberBinding>(n);
                    for (int j = 0; j < i; j++)
                    {
                        list.Add(original[j]);
                    }
                    list.Add(b);
                }
            }
            if (list != null)
                return list;
            return original;
        }

        protected virtual IEnumerable<ElementInit> VisitElementInitializerList(ReadOnlyCollection<ElementInit> original)
        {
            List<ElementInit> list = null;
            for (int i = 0, n = original.Count; i < n; i++)
            {
                ElementInit init = this.VisitElementInitializer(original[i]);
                if (list != null)
                {
                    list.Add(init);
                }
                else if (init != original[i])
                {
                    list = new List<ElementInit>(n);
                    for (int j = 0; j < i; j++)
                    {
                        list.Add(original[j]);
                    }
                    list.Add(init);
                }
            }
            if (list != null)
                return list;
            return original;
        }

        protected virtual Expression VisitLambda(LambdaExpression lambda)
        {
            Expression body = this.Visit(lambda.Body);
            if (body != lambda.Body)
            {
                return Expression.Lambda(lambda.Type, body, lambda.Parameters);
            }
            return lambda;
        }

        protected virtual NewExpression VisitNew(NewExpression nex)
        {
            IEnumerable<Expression> args = this.VisitExpressionList(nex.Arguments);
            if (args != nex.Arguments)
            {
                if (nex.Members != null)
                    return Expression.New(nex.Constructor, args, nex.Members);
                else
                    return Expression.New(nex.Constructor, args);
            }
            return nex;
        }

        protected virtual Expression VisitMemberInit(MemberInitExpression init)
        {
            NewExpression n = this.VisitNew(init.NewExpression);
            IEnumerable<MemberBinding> bindings = this.VisitBindingList(init.Bindings);
            if (n != init.NewExpression || bindings != init.Bindings)
            {
                return Expression.MemberInit(n, bindings);
            }
            return init;
        }

        protected virtual Expression VisitListInit(ListInitExpression init)
        {
            NewExpression n = this.VisitNew(init.NewExpression);
            IEnumerable<ElementInit> initializers = this.VisitElementInitializerList(init.Initializers);
            if (n != init.NewExpression || initializers != init.Initializers)
            {
                return Expression.ListInit(n, initializers);
            }
            return init;
        }

        protected virtual Expression VisitNewArray(NewArrayExpression na)
        {
            IEnumerable<Expression> exprs = this.VisitExpressionList(na.Expressions);
            if (exprs != na.Expressions)
            {
                if (na.NodeType == ExpressionType.NewArrayInit)
                {
                    return Expression.NewArrayInit(na.Type.GetElementType(), exprs);
                }
                else
                {
                    return Expression.NewArrayBounds(na.Type.GetElementType(), exprs);
                }
            }
            return na;
        }

        protected virtual Expression VisitInvocation(InvocationExpression iv)
        {
            IEnumerable<Expression> args = this.VisitExpressionList(iv.Arguments);
            Expression expr = this.Visit(iv.Expression);
            if (args != iv.Arguments || expr != iv.Expression)
            {
                return Expression.Invoke(expr, args);
            }
            return iv;
        }
    }
    #endregion

    public static class ReflectionExtensions
    {
        /// <summary>
        /// Extension for 'Object' that copies the properties to a destination object.
        /// </summary>
        /// <param name="source">The source.</param>
        /// <param name="destination">The destination.</param>
        public static void CopyProperties(this object source, object destination)
        {
            // If any this null throw an exception
            if (source == null || destination == null)
                throw new Exception("Source or/and Destination Objects are null");
            // Getting the Types of the objects
            Type typeDest = destination.GetType();
            Type typeSrc = source.GetType();

            // Iterate the Properties of the source instance and  
            // populate them from their desination counterparts  
            PropertyInfo[] srcProps = typeSrc.GetProperties();
            foreach (PropertyInfo srcProp in srcProps)
            {
                if (!srcProp.CanRead)
                {
                    continue;
                }
                PropertyInfo targetProperty = typeDest.GetProperty(srcProp.Name);
                if (targetProperty == null)
                {
                    continue;
                }
                if (!targetProperty.CanWrite)
                {
                    continue;
                }
                if ((targetProperty.GetSetMethod().Attributes & MethodAttributes.Static) != 0)
                {
                    continue;
                }
                if (!targetProperty.PropertyType.IsAssignableFrom(srcProp.PropertyType))
                {
                    continue;
                }
                // Passed all tests, lets set the value
                targetProperty.SetValue(destination, srcProp.GetValue(source, null), null);
            }
        }

        public static void Transfer(this object source, object target)
        {
            var sourceType = source.GetType();
            var targetType = target.GetType();

            var sourceParameter = Expression.Parameter(typeof(object), "source");
            var targetParameter = Expression.Parameter(typeof(object), "target");

            var sourceVariable = Expression.Variable(sourceType, "castedSource");
            var targetVariable = Expression.Variable(targetType, "castedTarget");

            var expressions = new List<Expression>();

            expressions.Add(Expression.Assign(sourceVariable, Expression.Convert(sourceParameter, sourceType)));
            expressions.Add(Expression.Assign(targetVariable, Expression.Convert(targetParameter, targetType)));

            foreach (var property in sourceType.GetProperties(BindingFlags.Public | BindingFlags.Instance))
            {
                if (!property.CanRead)
                    continue;

                var targetProperty = targetType.GetProperty(property.Name, BindingFlags.Public | BindingFlags.Instance);
                if (targetProperty != null
                        && targetProperty.CanWrite
                        && targetProperty.PropertyType.IsAssignableFrom(property.PropertyType))
                {
                    expressions.Add(
                        Expression.Assign(
                            Expression.Property(targetVariable, targetProperty),
                            Expression.Convert(
                                Expression.Property(sourceVariable, property), targetProperty.PropertyType)));
                }
            }

            var lambda =
                Expression.Lambda<Action<object, object>>(
                    Expression.Block(new[] { sourceVariable, targetVariable }, expressions),
                    new[] { sourceParameter, targetParameter });

            var del = lambda.Compile();

            del(source, target);
        }
    }
}
