﻿using System;
using System.IO;
using System.Data;
using System.IO.Compression;

namespace Corp.Framework.Helper
{
    public class DataCompressor
    {
        public static Byte[] Compress(DataTable dt)   
        {   
            Byte[] data;   
            MemoryStream memory = new MemoryStream();
            GZipStream zip = new GZipStream(memory, CompressionMode.Compress);              
            dt.WriteXml(zip, XmlWriteMode.WriteSchema);  
            zip.Close(); 
            data = memory.ToArray();  
            memory.Close();   
            return data;  
        }

        public static DataTable Decompress(Byte[] data) 
        { 
            MemoryStream memmory = new MemoryStream(data);
            GZipStream zip = new GZipStream(memmory, CompressionMode.Decompress);
            DataTable dt = new DataTable(); 
            dt.ReadXml(zip); 
            zip.Close();
            memmory.Close();
            return dt;

        }
    }
}
