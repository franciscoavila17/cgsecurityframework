﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Configuration;

namespace CG.lib.SecurityFramework.Mail
{
    public static class Mail
    {


        public static bool SendMail(string De, string Destinatario, string Mensaje, string Body, bool htmlBody,ref string outError) 
        {
            bool bolResultado = false;

            try
            {
                string mailServer = ConfigurationSettings.AppSettings["mail_servidor"];
                int mailServerPort = int.Parse( ConfigurationSettings.AppSettings["mail_puerto"]);

                MailMessage htmlMessage = new MailMessage(De, Destinatario,Mensaje,Body);
                SmtpClient smtpClient = new SmtpClient(mailServer, mailServerPort);

                htmlMessage.IsBodyHtml = htmlBody;
                smtpClient.Send(htmlMessage);

                bolResultado = true;
            }
            catch ( Exception err )
            {
                outError = string.Format("<Framework.Mail> {0}",err.Message);
            }


            return bolResultado;
        }


        public static bool SendMail(string De, string Destinatario, string Mensaje, string Body, bool htmlBody, string Adjunto, ref string outError)
        {
            bool bolResultado = false;

            try
            {
                string mailServer = ConfigurationSettings.AppSettings["mail_servidor"];
                int mailServerPort = int.Parse(ConfigurationSettings.AppSettings["mail_puerto"]);

                MailMessage htmlMessage = new MailMessage(De, Destinatario, Mensaje, Body);
                Attachment item = new Attachment(Adjunto);
                SmtpClient smtpClient = new SmtpClient(mailServer, mailServerPort);

                htmlMessage.IsBodyHtml = htmlBody;
                htmlMessage.Attachments.Add(item);

                smtpClient.Send(htmlMessage);
                bolResultado = true;
            }
            catch (Exception err)
            {
                outError = string.Format("<Framework.Mail> {0}", err.Message);
            }


            return bolResultado;
        }



        public static bool SendMail(string De, string Destinatario, string Mensaje, string Body, bool htmlBody, List<string> Adjuntos , ref string outError)
        {
            bool bolResultado = false;

            try
            {
                string mailServer = ConfigurationSettings.AppSettings["mail_servidor"];
                int mailServerPort = int.Parse(ConfigurationSettings.AppSettings["mail_puerto"]);

                MailMessage htmlMessage = new MailMessage(De, Destinatario, Mensaje, Body);                
                

                Adjuntos.ForEach( delegate(string adjunto) 
                {
                    Attachment item = new Attachment(adjunto);
                    htmlMessage.Attachments.Add(item);
                });


                SmtpClient smtpClient = new SmtpClient(mailServer, mailServerPort);

                htmlMessage.IsBodyHtml = htmlBody;
                

                smtpClient.Send(htmlMessage);
                bolResultado = true;
            }
            catch (Exception err)
            {
                outError = string.Format("<Framework.Mail> {0}", err.Message);
            }

            return bolResultado;
        }

        public static bool SendMail(string De, string Destinatario, List<string> CC, string Mensaje, string Body, bool htmlBody, List<string> Adjuntos, ref string outError)
        {
            bool bolResultado = false;

            try
            {
                string mailServer = ConfigurationSettings.AppSettings["mail_servidor"];
                int mailServerPort = int.Parse(ConfigurationSettings.AppSettings["mail_puerto"]);

                MailMessage htmlMessage = new MailMessage(De, Destinatario, Mensaje, Body);

                CC.ForEach(delegate(string cc)
                {
                    MailAddress item = new MailAddress(cc);
                    htmlMessage.CC.Add(item);
                });

                Adjuntos.ForEach(delegate(string adjunto)
                {
                    Attachment item = new Attachment(adjunto);
                    htmlMessage.Attachments.Add(item);
                });


                SmtpClient smtpClient = new SmtpClient(mailServer, mailServerPort);

                htmlMessage.IsBodyHtml = htmlBody;


                smtpClient.Send(htmlMessage);
                bolResultado = true;
            }
            catch (Exception err)
            {
                outError = string.Format("<Framework.Mail> {0}", err.Message);
            }

            return bolResultado;
        }

        public static bool SendMail(string De, List<string> Destinatario, List<string> CC, List<string> CCO, string Mensaje, string Body, bool htmlBody, List<string> Adjuntos, ref string outError)
        {
            bool bolResultado = false;

            try
            {
                string mailServer = ConfigurationSettings.AppSettings["mail_servidor"];
                int mailServerPort = int.Parse(ConfigurationSettings.AppSettings["mail_puerto"]);



                using (MailMessage htmlMessage = new MailMessage(De, Destinatario.FirstOrDefault() , Mensaje, Body))
                {

                    Destinatario.ForEach(delegate(string to)
                    {
                        if (to!= Destinatario.FirstOrDefault() )
                        {
                            MailAddress item = new MailAddress(to);
                            htmlMessage.To.Add(item);
                        }
                    });


                    CC.ForEach(delegate(string cc)
                    {
                        MailAddress item = new MailAddress(cc);
                        htmlMessage.CC.Add(item);
                    });

                    CCO.ForEach(delegate(string cco)
                    {
                        MailAddress item = new MailAddress(cco);
                        htmlMessage.Bcc.Add(item);
                    });

                    Adjuntos.ForEach(delegate(string adjunto)
                    {
                        Attachment item = new Attachment(adjunto);
                        htmlMessage.Attachments.Add(item);
                    });

                    

                    SmtpClient smtpClient = new SmtpClient(mailServer, mailServerPort);

                    htmlMessage.IsBodyHtml = htmlBody;

                    smtpClient.Send(htmlMessage);

                    foreach (Attachment attachment in htmlMessage.Attachments)
                    {
                        attachment.Dispose();                        
                    }


                    htmlMessage.Dispose();

                    smtpClient.Dispose();
                }

                bolResultado = true;

            }
            catch (Exception err)
            {
                outError = string.Format("<Framework.Mail> {0}", err.Message);
            }

            return bolResultado;
        }

    }
}
